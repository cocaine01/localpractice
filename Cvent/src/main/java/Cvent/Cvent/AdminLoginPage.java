package Cvent.Cvent;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import Utilities.Helper;



public class AdminLoginPage extends Base{
	
	WebDriver driver = null ; //Declared and Initializized
	
	@FindBy(xpath="//input[@id='user_login']")
	WebElement user_name;	
	
	@FindBy(xpath="//input[@id='user_pass']")
	WebElement password;
	
	@FindBy(xpath="//input[@id='wp-submit']")
	WebElement LoginBtn;
	
	@FindBy(xpath="//h1[text()='Dashboard']")
	WebElement DashBoard;
	
	public AdminLoginPage(WebDriver driver){
		//PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	//**********Credentials on Admin Page***********///
//	@Test(dataProvider="credentials")
		public boolean LoginMethod(String u_name, String pwd) throws IOException{
			
			Helper.highLightElement(user_name);
			user_name.sendKeys(u_name);
			Helper.highLightElement(password);
			password.sendKeys(pwd);
			child.info("Details of the credentials added");
			//ScreenShot("Details of the credentials added");
			ScreenShot();
			Helper.highLightEleAndClick(LoginBtn);
			//System.out.println(driver.getWindowHandle());
			Helper.ExplicitWaitVisibility(DashBoard);
			Helper.highLightElement(DashBoard);
			child.info(MarkupHelper.createLabel("Login Button Clicked , so Home Page Displayed", ExtentColor.CYAN));
			ScreenShot();			
			//return new LandingPage(driver);
			return true;  // add actu
		}
		
		/***************Data Provide****************/
		//    @DataProvider(name="credentials")
		//	   public Object[][] passData()
		//	     {
		//		Object [][] data= new Object[4][2];
		//		data[0][0]="TWDEV32";
		//		data[0][1]="cybage@123";
		//		
		//		data[1][0]="CODEV33";
		//		data[1][1]="cybage@123";	
		//		
		//		
		//		return data;
		//	    }

}
