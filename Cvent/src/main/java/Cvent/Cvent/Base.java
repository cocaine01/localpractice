package Cvent.Cvent;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.screentaker.ViewportPastingStrategy;
import Utilities.GetScreenshotExtent;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.model.ScreenCapture;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class Base {

	public static Properties CONFIG= null;
	public static String browser = null;
	public static WebDriver driver = null;

	//**********Configuration file*****************	
	public static void loadProperties(){

		try {
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\config\\config.properties");
			CONFIG = new Properties();
			CONFIG.load(fis);
			fis.close();
		} catch (FileNotFoundException e) 
		{
     		e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
	///**********Browser Initialization********************
	@SuppressWarnings("deprecation")
	public static WebDriver getBrowser(String browser){

		if(browser.equalsIgnoreCase("Chrome")){
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");

			ChromeOptions options = new ChromeOptions(); 
			options.addArguments("disable-infobars");
			options.addArguments("--start-maximized");
			options.addArguments("--ignore-certificate-errors");
			options.addArguments("--disable-popup-blocking");
			options.addArguments("--incognito");
			options.addArguments("disable-notifications");
			driver = new ChromeDriver(options);
		}
		if(browser.equalsIgnoreCase("InternetExplorer")){

			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"\\drivers\\IEDriverServer.exe");
			InternetExplorerOptions options = new InternetExplorerOptions();
			options.setCapability("ie.ensureCleanSession", true);
			options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			options.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, true);
			driver = new InternetExplorerDriver(options);
		}
		if(browser.equalsIgnoreCase("Mozilla")) {	
			System.setProperty("webdriver.gecko.driver", "C:\\Users\\sumeetk\\git\\Cvent\\Cvent\\drivers\\geckodriver.exe");
			//driver = new MarionetteDriver();
			//System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"\\drivers\\geckodriver.exe");
			driver= new FirefoxDriver();
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver;
	}
	//**********Initializing the page***********************
	public static void init(){
		loadProperties();
	}

	//***********ScreenShot Functionality*******************///
	public static ArrayList<String> ss_paths = new ArrayList<String>();


	public static void ScreenShot() throws IOException {		
		Calendar calender = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

		//*****Taking the screenshot
		File image = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		//Path where to copy the taken Screenshot and also the name	
		String actualImageName = "Screenshot_"+formatter.format(calender.getTime())+".png";		
		String path = System.getProperty("user.dir")+"\\test-output-extent\\screenshots\\"+actualImageName;
		ss_paths.add(path);
		File destFile = new File(path);
		//Final Copying the file to destination
		FileUtils.copyFile(image, destFile);
		child.log(Status.PASS, MarkupHelper.createLabel(" Screen Shot ", ExtentColor.TRANSPARENT));
		child.log(Status.INFO, "", MediaEntityBuilder.createScreenCaptureFromPath(path).build());
		//child.info("Rahul Ravish");

	}
	public static void ExtendedScreenShot() throws IOException {		
		Calendar calender = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
		//*****Taking the screenshot
		Screenshot fpScreenshot = new AShot().shootingStrategy(new ViewportPastingStrategy(1000)).takeScreenshot(driver);		
		BufferedImage image = fpScreenshot.getImage();
		//Path where to copy the taken Screenshot and also the name	
		String actualImageName = "SS_"+formatter.format(calender.getTime())+".png";		
		String path = System.getProperty("user.dir")+"\\test-output-extent\\screenshots\\"+actualImageName;
		ss_paths.add(path);
		File destFile = new File(path);

		//Final Copying the file to destination
		ImageIO.write(image, "PNG", destFile);

		child.log(Status.PASS, MarkupHelper.createLabel("Extended Whole Page Screen Shot for better clarity : Evidence of above step", ExtentColor.BROWN));
		child.log(Status.INFO, "", MediaEntityBuilder.createScreenCaptureFromPath(path).build());

		JavascriptExecutor jse1 = (JavascriptExecutor)driver;
		jse1.executeScript("scroll(250, 0)");

	}

	/*************Actions Class****/

	//Extent reports implemenation

	public static ExtentReports extent =null;
	public ExtentTest parent =  null; //Left hand side
	public static ExtentTest child =  null;

	@BeforeSuite
	public void beforeSuite() throws IOException {
		extent = new ExtentReports();
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "\\test-output-extent\\CventExtent.html");
		extent.attachReporter(htmlReporter);


		htmlReporter.config().setDocumentTitle("Cvent DevHub Automation Test results");
		htmlReporter.config().setTheme(Theme.DARK);
		htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		htmlReporter.config().setReportName("Cvent DevHub Automation Test Results");

	}

	@BeforeClass
	public synchronized void beforeClass() {
		parent = extent.createTest(getClass().getName());

	}

	@BeforeMethod
	public synchronized void beforeMethod(Method method) {
		child = parent.createNode(method.getName());
	}

	@AfterMethod
	public synchronized void afterMethod(ITestResult result) throws IOException {

		/***************/
		if (result.getStatus() == ITestResult.FAILURE){
			//Addedcode:-
			//String screenshotPath = GetScreenshotExtent.capture(driver);
			//
			child.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+ " FAILED due to below issue", ExtentColor.RED));
			child.fail(result.getThrowable());
			//child.addScreenCaptureFromPath(screenshotPath);
			//Commented
			Iterator<String> itr =  ss_paths.iterator();
			while(itr.hasNext()){				
				child.addScreenCaptureFromPath((String)itr.next());				
			}
			ss_paths.clear();///clearing the ss						
		}
		else if (result.getStatus() == ITestResult.SKIP)
			child.skip(result.getThrowable());
		else{
			child.log(Status.PASS, MarkupHelper.createLabel(result.getName()+ " :::test case Passed ", ExtentColor.GREEN));
			//child.pass("Test passed");
			//			Iterator<String> itr =  ss_paths.iterator();
			//			while(itr.hasNext()){				
			//					child.addScreenCaptureFromPath((String)itr.next());
			//				} 
		}
		ss_paths.clear();
	}	

	@AfterSuite
	public synchronized void afterSutie() throws IOException {
		/*********************/
		FileInputStream fis1 = new FileInputStream(System.getProperty("user.dir")+"\\config\\config.properties");
		Properties CONFIG1 = new Properties();
		CONFIG1.load(fis1);	
		String envType = null;
		String env = CONFIG1.getProperty("url");
		if(env.equalsIgnoreCase("https://apidev.cvent.com/1t1d-admin")){
			envType="API DEV Env";				
		}
		if(env.equalsIgnoreCase("https://stg-developers.cvent.com/1t1d-admin/")){
			envType="Staging Env";
		}
		if(env.equalsIgnoreCase("https://developers.cvent.com/wp-admin/")){
			envType="Production Env";
		}
		extent.setSystemInfo("Env",envType);
		//*******************///////
		String browser = null;
		String browserType = CONFIG1.getProperty("browser");
		if(browserType.equalsIgnoreCase("Chrome")){
			browser="Chrome";				
		}
		if(browserType.equalsIgnoreCase("Mozilla")){
			browser="Mozilla";				
		}
		if(browserType.equalsIgnoreCase("InternetExplorer")){
			browser="InternetExplorer";				
		}
		extent.setSystemInfo("BROWSER",browser );
		
		extent.flush();
	}

}





