package Cvent.Cvent;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.screentaker.ViewportPastingStrategy;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class BaseV1 {
	
	public static Properties CONFIG= null;
	public static String browser = null;
	public static WebDriver driver = null;
	
	//**********Configuration file*****************	
	public static void loadProperties(){
		
		try {
			FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\config\\config.properties");
			CONFIG = new Properties();
			CONFIG.load(fis);
			fis.close();
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
	///**********Browser Initialization********************
	@SuppressWarnings("deprecation")
	public static WebDriver getBrowser(String browser){
		if(browser.equalsIgnoreCase("Mozilla")){
			driver = new FirefoxDriver();			
		}
		if(browser.equalsIgnoreCase("Chrome")){
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
			
		    ChromeOptions options = new ChromeOptions(); 
			options.addArguments("disable-infobars"); 
			driver = new ChromeDriver(options);
			
			/*
			 * 
			 */
//			final DesiredCapabilities dc = DesiredCapabilities.chrome();
//			dc.setCapability(ChromeOptions.CAPABILITY, new ChromeOptions() {
//				{
//					setExperimentalOption("mobileEmulation", new HashMap<String, Object>() {
//						{
//							put("deviceName", "Nexus 5X");
//						}
//					});
//				}
//			});

//            ChromeDriver driver = new ChromeDriver(dc);
			 
		////	 
		}
		if(browser.equalsIgnoreCase("InternetExplorer")){
			
			System.setProperty("webdriver.ie.driver", System.getProperty("user.dir")+"\\drivers\\IEDriverServer.exe");
						
			//driver = new InternetExplorerDriver();
			//DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			//capabilities.setCapability("ie.ensureCleanSession", true);
			//capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			//driver = new InternetExplorerDriver(capabilities );
			
			InternetExplorerOptions options = new InternetExplorerOptions();
			options.setCapability("ie.ensureCleanSession", true);
			options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			options.setCapability(CapabilityType.ENABLE_PERSISTENT_HOVERING, true);
			
			driver = new InternetExplorerDriver(options);

				
			
			
			
//			val dc = DesiredCapabilities.internetExplorer()
//				    dc.setJavascriptEnabled(true)
//				     dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true)
//				    dc.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true)
//				    dc.setCapability(InternetExplorerDriver.ENABLE_ELEMENT_CACHE_CLEANUP, true)
//
//				    dc.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);

			
			
		}
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver;
	}
	//**********Initializing the page***********************
	public static void init(){
		loadProperties();
	}
	
	
	//***********ScreenShot Functionality*******************///
//	public static Random random = new Random(1000);
//	public static ArrayList<String> ss_paths = new ArrayList<String>();
//	Calendar calendar =Calendar.getInstance();
//	SimpleDateFormat formater =new  SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
//		
//   public static void  ScreenShot(){
//	   TakesScreenshot ts =  ((TakesScreenshot)driver);	  
//	   File raw =  ts.getScreenshotAs(OutputType.FILE);
//	  // File srcFile =  ts.getScreenshotAs(OutputType.FILE);	   
//	   int name = random.nextInt();
//	   String path=  System.getProperty("user.dir")+"\\test-output-extent\\screenshots\\"+name+".jpg" ;
//	   //String path=  System.getProperty("users.dir")+"\\screenshots\\"+name+".jpg";
//	   ss_paths.add(path);
//	   
//	   try {
//		FileUtils.copyFile(raw, new File(path));
//	} catch (IOException e) {
//		
//		e.printStackTrace();
//	}
//   }
	public static ArrayList<String> ss_paths = new ArrayList<String>();

	public static void ScreenShot() throws IOException {		
		Calendar calender = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");

		//*****Taking the screenshot
		//File image = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		
		Screenshot fpScreenshot = new AShot().shootingStrategy(new ViewportPastingStrategy(1000)).takeScreenshot(driver);		
		BufferedImage image = fpScreenshot.getImage();
		//ImageIO.write(fpScreenshot.getImage(),"PNG",new File("D:///FullPageScreenshot.png")); 				
		//File image = fpScreenshot.getScreenshotAs(OutputType.FILE);


		//Path where to copy the taken Screenshot and also the name	
		String actualImageName = "Screenshot_"+formatter.format(calender.getTime())+".png";		
		String path = System.getProperty("user.dir")+"\\test-output-extent\\screenshots\\"+actualImageName;
		ss_paths.add(path);
		File destFile = new File(path);

		//Final Copying the file to destination
		//FileUtils.copyFile(image, destFile);
		ImageIO.write(image, "PNG", destFile);

	}
   
   /*************Actions Class****/
  
   
   
   
   
   
   
   
   
   
   
 //Extent reports implemenation
   
   public static ExtentReports extent =null;
   public ExtentTest parent =  null; //Left hand side
   public static ExtentTest child =  null;
   
 	@BeforeSuite
 	public void beforeSuite() {
 		extent = new ExtentReports();
 		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "\\test-output-extent\\CventExtent.html");
 		extent.attachReporter(htmlReporter);
 	}
 	
     @BeforeClass
     public synchronized void beforeClass() {
         parent = extent.createTest(getClass().getName());
     }

     @BeforeMethod
     public synchronized void beforeMethod(Method method) {
         child = parent.createNode(method.getName());
     }

     @AfterMethod
     public synchronized void afterMethod(ITestResult result) {
         if (result.getStatus() == ITestResult.FAILURE){
         	child.fail(result.getThrowable());
         	Iterator<String> itr =  ss_paths.iterator();
     	while(itr.hasNext()){
     		try {
 				child.addScreenCaptureFromPath((String)itr.next());
 			} catch (IOException e) {
 				// TODO Auto-generated catch block
 				e.printStackTrace();
 			}
     	}
     	ss_paths.clear();///clearing the ss
     }
         else if (result.getStatus() == ITestResult.SKIP)
             child.skip(result.getThrowable());
         else{
             child.pass("Test passed");
         	Iterator<String> itr =  ss_paths.iterator();
         	while(itr.hasNext()){
         		try {
 					child.addScreenCaptureFromPath((String)itr.next());
 				} catch (IOException e) {
 					
 					e.printStackTrace();
 				}
         	}
         	ss_paths.clear();
         }	
     }
     
     @AfterSuite
     public synchronized void afterSutie() {
     	extent.flush();
     }
	
	   
	  
	   
	   
	   
	   
	   
	   
   }
	
	
	
 

