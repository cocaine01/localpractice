package Cvent.Cvent;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import Utilities.Helper;

public class BasicPostCreation extends Base {

	WebDriver driver = null;

	@FindBy(xpath="//div[text()='Posts']")
	WebElement PostsButton;

	@FindBy(xpath="//a[text()='Add New'][@class='page-title-action']")
	WebElement addNewBtn;

	@FindBy(xpath="//input[@type='text'][@name='post_title']")
	WebElement postTitleField;

	@FindBy(xpath="//body[@data-id='content']")
	WebElement ContentPage;

	//Status Selection
	@FindBy(xpath="//input[@id='save-post']")
	WebElement SaveDraftBtn;

	@FindBy(xpath="//a[@id='post-preview']")
	WebElement PreviewBtn;

	@FindBy(xpath="//button[@id='insert-media-button']")
	WebElement AddMediaBtn;


	@FindBy(xpath="//button[@id='__wp-uploader-id-1']")
	WebElement SelectFileBtn;

	@FindBy(xpath="//button[text()='Insert into post']")
	WebElement INsertIntoPOstBtn;

	@FindBy(xpath="//a[text()='Upload Files']")
	WebElement UploadFilesLink;

	@FindBy(xpath="//body[@id='tinymce']/p")
	WebElement ContentBase;

	@FindBy(xpath="//a[text()='All Posts']")
	WebElement AllPostsBtn;	

	@FindBy(xpath="//div[@class='postbox closed']/child::h2/span[text() = 'Publish']")
	//span[text() = 'Publish']
	WebElement publishWindow;
	
	@FindBy(xpath="//a[@class='ab-item'][@aria-haspopup]/span[@class='display-name']")
	WebElement Name;
	
	@FindBy(xpath="//a[@class='ab-item'][text()='Log Out']")
	WebElement LogOutBtn;
	


	public BasicPostCreation(WebDriver driver){
		//PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	@SuppressWarnings("resource")
	public boolean PostCreationMtd(String heading) throws IOException{

		//child.log(Status.PASS, MarkupHelper.createLabel(" Clicking on Posts Button ", ExtentColor.CYAN));	
		child.info("Clicking on Posts Button");
		Helper.highLightEleAndClick(PostsButton);
		ExtendedScreenShot();
		
		child.info("Clicking on 'Add New' Button ");
		//child.log(Status.PASS, MarkupHelper.createLabel("Clicking on 'Add New' Button ", ExtentColor.CYAN));	
		Helper.highLightEleAndClick(addNewBtn);
		Helper.ExplicitWait(postTitleField);
		
		ScreenShot();
		child.info(MarkupHelper.createLabel("Entering the Subject Line", ExtentColor.CYAN));
		
		Helper.highLightEleAndSendKeys(postTitleField, "Lets' Test Automation for "+heading);
		ScreenShot();
		
		//child.info(MarkupHelper.createLabel("Entering the Plain Content", ExtentColor.CYAN));
		child.info("Entering the Plain Content");
		/* //***OLd way for entering the text****/
		StringBuffer sb = new StringBuffer();
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\Data\\Programs.txt");
		int data = fis.read();
		while(data!=-1){
			char c = ((char)data);
			data =fis.read();
			sb.append(c);			
		}

		driver.switchTo().frame( driver.findElement(By.xpath("//iframe[@id = 'content_ifr']")));		
		WebElement Para =driver.findElement(By.xpath("//body[@id='tinymce']"));
		Helper.highLightEleAndClick(Para);
		//Para.click();
		Para.sendKeys(sb);
		ExtendedScreenShot();	
		

		//Actions action = new Actions(driver);
		//action.moveToElement(driver.findElement(By.xpath("//html/body"))).click().build().perform();		
		//action.sendKeys(sb).build().perform();
		driver.switchTo().defaultContent();
		child.info(MarkupHelper.createLabel("Clicking on Save Draft Button", ExtentColor.CYAN));
				
		if(SaveDraftBtn.isDisplayed()){
			//code here to click 'Save as Draft Btn'            
			Helper.moveToElementAndClick(SaveDraftBtn);	
			//					Actions actions = new Actions(driver);
			//					actions.moveToElement(SaveDraftBtn).click().perform();
		}else{
			//code here to click on Public drop down
			Helper.highLightEleAndClick(publishWindow);
			//publishWindow.click();
			
			Helper.moveToElementAndClick(SaveDraftBtn);
			
			//Helper.ExplicitWait(SaveDraftBtn);
			//SaveDraftBtn.click();					
		}
		//				SaveDraftBtn.click();
		//				Helper.ExplicitWait(SaveDraftBtn);
		Helper.highLightEleAndClick(AllPostsBtn);
		ScreenShot();
		Helper.ExplicitWaitVisibility(Name);
		
		child.log(Status.PASS, MarkupHelper.createLabel("Logging out", ExtentColor.CYAN));
		Helper.moveToElement(Name);
		Helper.ExplicitWaitVisibility(LogOutBtn);
		child.info(MarkupHelper.createLabel("Logout button Clicked", ExtentColor.LIME));
		
		LogOutBtn.click();
		ScreenShot();

		return true;


	}

}
