package Cvent.Cvent;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Utilities.Helper;


public class BreakingDocumentation extends Base{

	WebDriver driver = null;

	@FindBy(xpath="")
	WebElement x ;

	@FindBy(xpath="//li[@id='menu-item-136']//a[text()='Documentation']")
	WebElement  DocumentationHeader;
	
	@FindBy(xpath="//li[@id='menu-item-136']//a[text()='Products']")
	WebElement ProductsDropdown ;
	

	@FindBy(xpath="//li[@id='menu-item-136']//a[text()='Event Management']")
	WebElement EvenManagement ;


	@FindBy(xpath="//li[@id='menu-item-136']//a[text()='Passkey']")
	WebElement Passkey ;

	@FindBy(xpath="//ul[@class='toc-expandable page-doc mCustomScrollbar _mCS_1 mCS_no_scrollbar']")
	WebElement MenuItemsUncollapsed ;

	@FindBy(xpath="//div[@class='entry-content clearfix']")
	WebElement contentOfDoc;

	//	@FindBy(xpath="")
	//	WebElement x ;

	public BreakingDocumentation(WebDriver driver){
		this.driver =driver;
	}



	public Boolean BrkMethod() throws InterruptedException {

		child.info("Navigating to the first Dropdown");

		Helper.moveToElement(ProductsDropdown);
		Helper.highLightEleAndClick(EvenManagement);
		//driver.get("https://developers.cvent.com/index.php/doc/event-management/");
		Thread.sleep(1000);
		Actions act = new Actions(driver);
		//WebDriverWait webDriverWait = new WebDriverWait(driver,5);

		String text = "Busted!";





		try{
			List<WebElement> li = driver.findElements(By.xpath("//div[@id='mCSB_1_container']/li"));

			for (int i = 0; i <= li.size()-1; i++)
			{
				act.moveToElement(li.get(i)).click().build().perform();
				Thread.sleep(2000);
				//webDriverWait.until(ExpectedConditions.visibilityOf(contentOfDoc));
				Assert.assertNotSame(contentOfDoc.getText(),text.equalsIgnoreCase("Busted!"));
				//li.get(i).click();			

				List<WebElement> subli = driver.findElements(By.xpath("//div[@id='mCSB_1_container']/li["+i+"+1]/ul/li"));
				for (int j = 0; j <= subli.size()-1; j++)
				{
					act.moveToElement(subli.get(j)).click().build().perform();
					Thread.sleep(2000);
					//webDriverWait.until(ExpectedConditions.visibilityOf(contentOfDoc));
					Assert.assertNotSame(contentOfDoc.getText(),text.equalsIgnoreCase("Busted!"));
					//subli.get(j).click();

					//Thread.sleep(2000);
					List<WebElement> subofsubli = driver.findElements(By.xpath("//div[@id='mCSB_1_container']/li["+i+"+1]/ul/li["+j+"+1]/ul/li"));
					for (int k = 0; k <= subofsubli.size()-1; k++)
					{
						act.moveToElement(subofsubli.get(k)).click().build().perform();
						Thread.sleep(2000);
						//webDriverWait.until(ExpectedConditions.visibilityOf(contentOfDoc));
						Assert.assertNotSame(contentOfDoc.getText(),text.equalsIgnoreCase("Busted!"));
						//subofsubli.get(k).click();
						//Thread.sleep(2000);
					}
					act.moveToElement(subli.get(j)).click().build().perform();
					Thread.sleep(2000);
					//webDriverWait.until(ExpectedConditions.visibilityOf(contentOfDoc));
					Assert.assertNotSame(contentOfDoc.getText(),text.equalsIgnoreCase("Busted!"));
					//subli.get(j).click();

				}
			}

		}
		catch(Exception e){
			System.out.println("Switchingh to new Page");
		}


		Helper.moveToElement(DocumentationHeader);
		Helper.highLightEleAndClick(Passkey);
		Thread.sleep(5000);


		try{
			List<WebElement> li2 = driver.findElements(By.xpath("//div[@id='mCSB_1_container']/li"));
			for (int i = 0; i <= li2.size()-1; i++)
			{
				act.moveToElement(li2.get(i)).click().build().perform();
				//webDriverWait.until(ExpectedConditions.visibilityOf(contentOfDoc));
				Assert.assertNotSame(contentOfDoc.getText(),text.equalsIgnoreCase("Busted"));

				//li.get(i).click();			
				Thread.sleep(2000);
				List<WebElement> subli2 = driver.findElements(By.xpath("//div[@id='mCSB_1_container']/li["+i+"+1]/ul/li"));
				for (int j = 0; j <= subli2.size()-1; j++)
				{
					act.moveToElement(subli2.get(j)).click().build().perform();
					//webDriverWait.until(ExpectedConditions.visibilityOf(contentOfDoc));
					Assert.assertNotSame(contentOfDoc.getText(),text.equalsIgnoreCase("Busted"));
					//subli.get(j).click();

					Thread.sleep(2000);
					List<WebElement> subofsubli2 = driver.findElements(By.xpath("//div[@id='mCSB_1_container']/li["+i+"+1]/ul/li["+j+"+1]/ul/li"));
					for (int k = 0; k <= subofsubli2.size()-1; k++)
					{
						act.moveToElement(subofsubli2.get(k)).click().build().perform();
						//webDriverWait.until(ExpectedConditions.visibilityOf(contentOfDoc));
						Assert.assertNotSame(contentOfDoc.getText(),text.equalsIgnoreCase("Busted"));
						//subofsubli.get(k).click();
						Thread.sleep(2000);
					}
					act.moveToElement(subli2.get(j)).click().build().perform();
					//webDriverWait.until(ExpectedConditions.visibilityOf(contentOfDoc));
					Assert.assertNotSame(contentOfDoc.getText(),text.equalsIgnoreCase("Busted"));
					//subli.get(j).click();

				}
			}

		}
		catch(Exception e){
			System.out.println("Done Traversing");
		}


		return true;
	}
}

