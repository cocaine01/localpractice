package Cvent.Cvent;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import Utilities.Helper;

public class ComplexDocCreation extends Base {

	public WebDriver driver = null;
	
	public ComplexDocCreation(WebDriver driver) {
		this.driver = driver;		
	}
	
	@FindBy(xpath="//a[text()='Add Documentation']")
	WebElement AddDocumentationBtn;

	@FindBy(xpath="//input[@id='title']")
	WebElement DocPageTitle;

	@FindBy(xpath="//div[@class='inside']//label[@class='selectit'][text()=' Event Management']")
	WebElement EventManagementChkBox;
	//String x= "//label[@class='selectit'][text()=' Event Management']"; 

	@FindBy(xpath="//div[@class='inside']//label[@class='selectit'][text()=' Father']")
	WebElement FatherBtn;

	@FindBy(xpath="//div[@class='inside']//label[@class='selectit'][text()=' Child']")
	WebElement ChildBtn;

	@FindBy(xpath="//div[@class='inside']//label[@class='selectit'][text()=' GrandChild']")
	WebElement GrandChildBtn;

	//WebElement BaseChkBox = driver.findElement(By.xpath("//label[@class='selectit'][text()=' "+CONFIG.getProperty("parentCategory")));
	//WebElement BaseChkBox1 =driver.findElement(By.xpath("//label[@class='selectit'][text()=' {CONFIG.getProperty("parentCategory")}']"));
	//WebElement BaseChkBox = driver.findElement(By.xpath("//label[@class='selectit'][text()=' "+CONFIG.getProperty("parentCategory")+"']"));

	@FindBy(xpath="//input[@value='Update'][@type='submit']")
	WebElement UpdateBtn;

	@FindBy(xpath="//input[@id='save-post']")
	WebElement SaveDraftBtn;
	//	
	@FindBy(xpath="//input[@value='Publish'][@type='submit']")
	WebElement PublishBtn;

	@FindBy(xpath="//select[@id='parent_id']")
	WebElement PageDroprDown1;

	@FindBy(xpath="//a[text()='Preview Changes']")
	WebElement PreviewChangesBtn;	

	@FindBy(xpath="//div[text()='Documentation']")
	WebElement DocumentationBtn;

	@FindBy(xpath="//ul[@id='list-manual']")
	WebElement PagesGrid;

	@FindBy(xpath="//a[@class='ab-item'][text()='Log Out']")
	WebElement LogOutBtn;

	@FindBy(xpath="//a[@class='ab-item'][@aria-haspopup]/span[@class='display-name']")
	WebElement Name;
    
	////////Adding Code for Complex content
	
//	@FindBy(xpath="")
//	WebElement x;
	@FindBy(xpath="//a[text()='Backend Editor']")
	WebElement BackEndEditorBtn;
	@FindBy(xpath="//a[@class='vc_control-set-column set_columns vc_active']/i[@class ='vc-composer-icon vc-c-icon-1-1']")
	WebElement FormatSetColumns;
	@FindBy(xpath="//a[@class='vc_control-set-column set_columns']//i[@class ='vc-composer-icon vc-c-icon-1-2_1-2']")
	WebElement OneByTwo;
	//div[@class='vc_welcome-visible-ne']//a[@title='Add Element']
	@FindBy(xpath="//a[@title='Add new element']")
	WebElement AddBlockBtn;
	@FindBy(xpath="//i[@class='vc_general vc_element-icon icon-wpb-layer-shape-text']")
	WebElement TextBlockBtn;
	@FindBy(xpath="//*[@id='vc_ui-panel-edit-element']/div[1]/div[3]/div/div/span[1]")
	WebElement CloseBtn;	
	@FindBy(xpath="//div[@class='wpb_column_container vc_container_for_children vc_empty-container ui-sortable']")
	WebElement AddContentBtn;
	@FindBy(xpath="//*[contains(text(),'ALL')]")
	WebElement ALLBtn;
	@FindBy(xpath="//*[@id='TS_Advanced_Tables']/span[2]")
	WebElement AddTableBtn;
	@FindBy(xpath="//select[@class='wpb_vc_param_value ts-advanced-tables-select-input id advanced_tables']")
	WebElement SelectTableNo;
	@FindBy(xpath="//div[@id='vc_ui-panel-edit-element']//div[@class='vc_ui-panel-footer-container']//child::span[2]")
	WebElement SaveChnagesBtn;
	
	
	
	@FindBy(xpath="//button[@class='vc_ui-tabs-line-trigger vc_add-element-filter-button'][contains(text(),'All')]")
	WebElement AllBtnINAddElement;
	
	//a[@id='vc_single_image']
	
	@FindBy(xpath="//a[@id='vc_single_image']")
	WebElement SingleImgBtn;
	
	@FindBy(xpath="//input[@class='wpb_vc_param_value wpb-textinput img_size textfield']")
	WebElement ImageSizeField;
	
	@FindBy(xpath="//div[@class='vc_welcome-visible-ne']//a[@title='Add Element']")
	WebElement AddNewContentTypeBtn;
	
	@FindBy(xpath="//li[@id='manualdocumentationcategory-7']//child::label[1]")
	WebElement EventManagementButton;
	
	@FindBy(xpath="//a[@class='preview button']")
	WebElement PreviewBtn;


	public boolean ComplexDocumentCreationMtd() throws IOException, InterruptedException {

		child.info("Updating Content Page Wise for the Event Management");
		DocumentationBtn.click();
		AddDocumentationBtn.click();
		Helper.ExplicitWait(DocPageTitle);
		DocPageTitle.click();
		DocPageTitle.sendKeys("Automated Document Page Creation for Testing the Content Capabilities");

		Helper.ExplicitWait(BackEndEditorBtn);
		BackEndEditorBtn.click();
		
		Helper.ExplicitWait(AddBlockBtn);
		AddBlockBtn.click();
		Helper.ExplicitWait(AllBtnINAddElement);
		AllBtnINAddElement.click();
		//Helper.SwitchToWindow(1);
		TextBlockBtn.click();
		
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@id='wpb_tinymce_content_ifr']")));
		WebElement Para =driver.findElement(By.xpath("//body[@id='tinymce']"));
		Para.clear();
		
		StringBuffer sb = new StringBuffer();
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\Data\\ComplexDocumentation.txt");
		int data = fis.read();
		while(data!=-1){
			char c = ((char)data);
			data =fis.read();
			sb.append(c);			
		}
		
		Para.sendKeys(sb);
		driver.switchTo().defaultContent();
		SaveChnagesBtn.click();		
		//CloseBtn.click();
				
		Helper.ExplicitWaitVisibility(FormatSetColumns);
			
		Helper.moveToElement(FormatSetColumns);
		OneByTwo.click();		
		AddContentBtn.click();
		AllBtnINAddElement.click();
		
	
		Helper.moveToElementAndClick(SingleImgBtn);
		driver.findElement(By.xpath("//a[@class='gallery_widget_add_images']")).click();
		
		//JavascriptExecutor js = (JavascriptExecutor) driver;		
	    Helper.SelectDrpDwn(driver.findElement(By.xpath("//div[@class='media-toolbar-secondary']//child::select[1]")), "unattached");
		
		Thread.sleep(5000);
		WebElement Element = driver.findElement(By.xpath("//img[@src='https://apidev.cvent.com/wp-content/uploads/2018/02/c5a84eff8753419d8fe1b0fb72dbb0df_LARGE.jpg']"));
		//act.moveToElement(.click().build().perform();
		//js.executeScript("arguments[0].scrollIntoView();", Element);
		Helper.ExplicitWaitVisibility(Element);
		
		Actions act = new Actions(driver);
		act.moveToElement(Element).click().build().perform();		
		
		driver.findElement(By.xpath("//button[text()='Set Image']")).click();
		Thread.sleep(5000);
		
		ImageSizeField.click();
		ImageSizeField.clear();
		ImageSizeField.sendKeys("large");
		
		
		Helper.ExplicitWaitVisibility(SaveChnagesBtn);
		act.moveToElement(SaveChnagesBtn).click().build().perform();
		//SaveChnagesBtn.click();
		
		System.out.println("Sumeet1");
		Thread.sleep(5000);
		
		
		//Helper.ExplicitWaitVisibility(AddNewContentTypeBtn);
		//AddNewContentTypeBtn.click();		
		act.moveToElement(AddNewContentTypeBtn).click().build().perform();
		System.out.println("Sumeet2");
		AllBtnINAddElement.click();		
		
		//TABLE ENTRY
		Helper.moveToElementAndClick(AddTableBtn);
		System.out.println("6");		
		Helper.ExplicitWaitVisibility(SelectTableNo);
		System.out.println("8");
		Helper.SelectDrpDwnText(SelectTableNo, "Event Management_SOAP API_Login/Client Session Call_Login_Output (ID#4)");
		//System.out.println("9");
		SaveChnagesBtn.click();
		
		EventManagementButton.click();
		Helper.ExplicitWaitVisibility(PreviewBtn);
		PreviewBtn.click();
		
		
		
		
		
		//******Adding the required content 
		
		
		
		
		
		
		
		
		
		
		
		
		
		/*child.info("Content for Event Management Created");
		ScreenShot();
		driver.switchTo().defaultContent();		
		EventManagementChkBox.click();		

		child.info("Publishing the content for Event Management");
		PublishBtn.click();*/


		
		



		return true;
	}
	
	

}
