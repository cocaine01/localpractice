package Cvent.Cvent;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import Utilities.Helper;

public class ContentOwnerCapabilities extends Base{
	
	WebDriver driver = null;
	
	@FindBy(xpath="//button[@value='draft']")
	WebElement SaveDraftBtn;
	
	@FindBy(xpath="//div[text()='Posts']")
	WebElement PostsButton;
		
	@FindBy(xpath="//a[text()='All Posts']")
	WebElement AllPostsBtn;	
	
	@FindBy(xpath="//input[@id='post-search-input']")
	WebElement PostSearchInputField;
	
	@FindBy(xpath="//input[@id='search-submit']")
	WebElement SearchPostButton ;
	
	@FindBy(xpath="//a[text()='Edit']")
	WebElement EditBtn;
	
	@FindBy(xpath="//input[@id='title']")
	WebElement Title;
	
	@FindBy(xpath="//a[@class='edit-post-status hide-if-no-js']")
	WebElement StatusEditBtn;
	@FindBy(xpath="//select[@id='post_status']")
	WebElement StatusDrpDwn;
	
	@FindBy(xpath="//a[text()='OK'][@class='save-post-status hide-if-no-js button']")
	WebElement OKBtn;
	
	@FindBy(xpath="//input[@value='Save as Pending']")
	WebElement SaveAsPendingBtn;
	
	@FindBy(xpath="//input[@value='Save as Reviewed']")
	WebElement SaveAsReviewedBtn;
	//
	@FindBy(xpath="//tbody[@id='the-list']/tr")
	WebElement MouseHoverElement;
	
	@FindBy(xpath="//a[@class='ab-item'][@aria-haspopup]/span[@class='display-name']")
	WebElement Name;
	
	@FindBy(xpath="//a[@class='ab-item'][text()='Log Out']")
	WebElement LogOutBtn;
	
	//New Changes for Tech Writer
	
	@FindBy(xpath="//input[@type='submit' and @id='publish']")
	WebElement SubmitForReviewBtn;
	
	@FindBy(xpath="//span[text()='Pending Review']")
	WebElement PostStatusPR;
	
	@FindBy(xpath="//span[text()='Draft']")
	WebElement PostStatusDraft;
	
	@FindBy(xpath="//span[text()='Reviewed']")
	WebElement PostStatusReviewed;
	
	@FindBy(xpath="//input[@value='Save Draft']")
	WebElement SaveDraftBtn1;
	
	
	public ContentOwnerCapabilities(WebDriver driver){
		this.driver=driver;
	}
	
	public boolean ContentOwnerCapabilitiesMtd(String heading) throws IOException{
		child.info("Clicking on All Posts Button");
		
		Helper.ExplicitWaitVisibility(PostsButton);
		Helper.highLightEleAndClick(PostsButton);
		Helper.highLightEleAndClick(AllPostsBtn);
		ExtendedScreenShot();
		Helper.ExplicitWait(PostSearchInputField);
		Helper.highLightEleAndClick(PostSearchInputField);				
		child.info(MarkupHelper.createLabel("Searching by the Post that was created in the Search Box", ExtentColor.CYAN));
		Helper.highLightEleAndSendKeys(PostSearchInputField, "Lets' Test Automation for "+heading);
		ScreenShot();
		child.info("Clicking on Search Post Button");
		System.out.println("Sumeet1");
		Helper.highLightEleAndClick(SearchPostButton);	
		System.out.println("Sumeet2");
		MouseHoverElement.click();		
		ScreenShot();
		
		child.log(Status.PASS, MarkupHelper.createLabel("Clicking on Edit Post", ExtentColor.CYAN));
		Helper.ExplicitWait(EditBtn);
		Helper.highLightEleAndClick(EditBtn);
		Helper.ExplicitWait(Title);
		ScreenShot();
		
		//*Changes for Content Owner//
		
		child.info(MarkupHelper.createLabel("Changing the Status Now to Pending review", ExtentColor.CYAN));
		StatusEditBtn.click();
		ScreenShot();
		child.info("Selecting from the dropdown");
		Helper.SelectDrpDwn(StatusDrpDwn, "pending");
		Helper.highLightEleAndClick(OKBtn);
        ScreenShot();
        child.info("Clicking on 'Save as Pending Review' button");
        Helper.ExplicitWait(SaveAsPendingBtn);
        SaveAsPendingBtn.click();      
       
        Helper.ExplicitWait(AllPostsBtn);
        child.info("Veriffying that the status is Pending Review displayed in the right hand side window");
        Assert.assertEquals(PostStatusPR.isDisplayed(), PostStatusPR.isDisplayed());
        ScreenShot();
        
		Helper.highLightEleAndClick(AllPostsBtn);
        Helper.ExplicitWait(PostSearchInputField);
        Helper.highLightEleAndClick(PostSearchInputField);
        child.info("Searching the previous post");
		Helper.highLightEleAndSendKeys(PostSearchInputField, "Lets' Test Automation for "+heading);
		Helper.highLightEleAndClick(SearchPostButton);		
		child.info(MarkupHelper.createLabel("New Status of the Post is now 'Pending'", ExtentColor.INDIGO));
	   	ScreenShot();
		//////////////////////////////////////
		child.info("Searching by the Post that was created in the Search Box");
		PostSearchInputField.clear();
		child.info("Entering the  Subject Line to search for");
		Helper.highLightEleAndSendKeys(PostSearchInputField, "Lets' Test Automation for "+heading);
		ScreenShot();
		child.info("Clicking on 'Search Post' Button");
		SearchPostButton.click();
		
		MouseHoverElement.click();		
		ScreenShot();
		
		child.info("Clicking on Edit Post");
		Helper.ExplicitWait(EditBtn);
		EditBtn.click();
		Helper.ExplicitWait(Title);
		ScreenShot();
		
		/////////////////////////////////////////////////////////////////////
		child.info(MarkupHelper.createLabel("Changing the Status Now to 'Reviewed' review", ExtentColor.CYAN));
		StatusEditBtn.click();
		ScreenShot();
		Helper.SelectDrpDwn(StatusDrpDwn, "reviewed");
		Helper.highLightEleAndClick(OKBtn);
               
        Helper.ExplicitWait(SaveAsReviewedBtn);
        Helper.highLightEleAndClick(SaveAsReviewedBtn);
        
        Helper.ExplicitWait(AllPostsBtn);
        child.info("Veriffying that the status is Reviewed displayed in the right hand side window");
        Assert.assertEquals(PostStatusReviewed.isDisplayed(), PostStatusReviewed.isDisplayed());
        ScreenShot();
		
        AllPostsBtn.click();
        Helper.ExplicitWait(PostSearchInputField);
        Helper.highLightEleAndClick(PostSearchInputField);
		//PostSearchInputField.click();
		Helper.highLightEleAndSendKeys(PostSearchInputField, "Lets' Test Automation for "+heading);
		Helper.highLightEleAndClick(SearchPostButton);
		
		child.info(MarkupHelper.createLabel("No Status of the Post means it is reviewed", ExtentColor.INDIGO));
		ScreenShot();
		
		
		
		/***********Reversing the Statuses*********/
		MouseHoverElement.click();		
		ScreenShot();
		child.log(Status.PASS, MarkupHelper.createLabel("Clicking on Edit Post", ExtentColor.CYAN));
		Helper.ExplicitWait(EditBtn);
		Helper.highLightEleAndClick(EditBtn);
		Helper.ExplicitWait(Title);
		ScreenShot();
		
		child.info(MarkupHelper.createLabel("Changing the Status Now to from Reviewed to Draft", ExtentColor.CYAN));
		StatusEditBtn.click();
		ScreenShot();
		Helper.SelectDrpDwn(StatusDrpDwn, "draft");
		Helper.highLightEleAndClick(OKBtn);
		
		Helper.ExplicitWait(SaveDraftBtn1);
		Helper.highLightEleAndClick(SaveDraftBtn1);
		
		Helper.ExplicitWait(AllPostsBtn);

        child.info(MarkupHelper.createLabel("Verifying that the status is 'Draft' displayed in the right hand side window", ExtentColor.INDIGO));
        Assert.assertEquals(PostStatusDraft.isDisplayed(), PostStatusDraft.isDisplayed());
        ScreenShot();
        
        //Now Changing to Draft to Reviwed
        
        child.info(MarkupHelper.createLabel("Now Changing the Status from Draft to Reviewed Directly", ExtentColor.CYAN));
        StatusEditBtn.click();
		ScreenShot();
		Helper.SelectDrpDwn(StatusDrpDwn, "reviewed");
		Helper.highLightEleAndClick(OKBtn);		
	
		Helper.ExplicitWait(SaveAsReviewedBtn);
		Helper.highLightEleAndClick(SaveAsReviewedBtn);
		
		Helper.ExplicitWait(AllPostsBtn);
		child.info(MarkupHelper.createLabel("Verifying that the status is 'Reviewed' displayed in the right hand side window", ExtentColor.INDIGO));
        Assert.assertEquals(PostStatusReviewed.isDisplayed(), PostStatusReviewed.isDisplayed());
        ScreenShot();
        
        
        ///
        child.info(MarkupHelper.createLabel("Now Changing the status from Reviewed to Pending Review", ExtentColor.CYAN));
        StatusEditBtn.click();
		ScreenShot();
		Helper.SelectDrpDwn(StatusDrpDwn, "pending");
		Helper.highLightEleAndClick(OKBtn);
		SaveAsPendingBtn.click();
		Helper.ExplicitWait(AllPostsBtn);
		child.info(MarkupHelper.createLabel("Verifying that the status is 'Pending Review' displayed in the right hand side window", ExtentColor.INDIGO));
        Assert.assertEquals(PostStatusPR.isDisplayed(), PostStatusPR.isDisplayed());
        ScreenShot();
        
        ///
        
        child.info(MarkupHelper.createLabel("Now Changing the status from Pending Review to Draft", ExtentColor.CYAN));
        StatusEditBtn.click();
		ScreenShot();
		Helper.SelectDrpDwn(StatusDrpDwn, "draft");
		Helper.highLightEleAndClick(OKBtn);
		SaveDraftBtn1.click();
		Helper.ExplicitWait(AllPostsBtn);
		child.info(MarkupHelper.createLabel("Verifying that the status is 'Draft' displayed in the right hand side window", ExtentColor.INDIGO));
        Assert.assertEquals(PostStatusDraft.isDisplayed(), PostStatusDraft.isDisplayed());
        ScreenShot();
        
		
	   //	Logging out**
		child.info("Logging out");
		Helper.moveToElement(Name);
		ScreenShot();
		child.info("Logging out");
		Helper.moveToElement(Name);
		Helper.ExplicitWaitVisibility(LogOutBtn);
		child.info(MarkupHelper.createLabel("Logout button Clicked", ExtentColor.LIME));
		
		LogOutBtn.click();
		ScreenShot();
		return true;
	}
	

}
