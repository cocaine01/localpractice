package Cvent.Cvent;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import Utilities.Helper;

public class DeletePosts extends Base {

	WebDriver driver = null;	

	@FindBy(xpath="//div[text()='Posts']")
	WebElement PostsButton;

	@FindBy(xpath="//input[@id='cb-select-all-1']")
	WebElement ChkBx;	

	@FindBy(xpath="//select[@id='bulk-action-selector-top']")
	WebElement ActionsDrpDwn;

	@FindBy(xpath="//input[@id='doaction']")
	WebElement ApplyBtn;

	@FindBy(xpath="//a[text()='All Posts']")
	WebElement AllPostsBtn;	

	@FindBy(xpath="//input[@id='post-search-input']")
	WebElement PostSearchInputField;

	@FindBy(xpath="//input[@id='search-submit']")
	WebElement SearchPostButton ;

	@FindBy(xpath="//div[@class='wp-menu-name']")
	WebElement DashBoard;

	@FindBy(xpath="//a[text()='Add Documentation']")
	WebElement AddDocumentationBtn;

	@FindBy(xpath="//div[text()='Documentation']")
	WebElement DocumentationBtn;

	@FindBy(xpath="//a[text()='All Documentation']")
	WebElement AllDocumentationBtn;

	@FindBy(xpath="//a[text()='Documentation Categories']")
	WebElement DocumentationCategoriesBtn;

	@FindBy(xpath="//input[@id='tag-search-input']")
	WebElement DocumentatioCategorySearchInputField;

	@FindBy(xpath="//input[@id='search-submit']")
	WebElement SearchDocBtnBtn;



	@FindBy(xpath="//td[text()='No Documentation found']")
	WebElement NoDocFound;


	@FindBy(xpath="//td[text()='No posts found.']")
	WebElement NoPostFound;

	@FindBy(xpath="//a[@class='ab-item'][@aria-haspopup]/span[@class='display-name']")
	WebElement Name;

	@FindBy(xpath="//a[@class='ab-item'][text()='Log Out']")
	WebElement LogOutBtn;



	@FindBy(xpath="//td[text()='No posts found.']")
	WebElement Nopostsfound;

	public DeletePosts(WebDriver driver) {
		this.driver = driver;
	}

	public boolean PostDeleteMtd() throws IOException{
		///Posts deletion
		Helper.ExplicitWaitVisibility(DashBoard);
		child.info("Clicking on All Posts Button");
		PostsButton.click();
		AllPostsBtn.click();
		ScreenShot();
		Helper.ExplicitWait(PostSearchInputField);
		PostSearchInputField.click();

		child.info(MarkupHelper.createLabel("Entering by the Post that was created in thee Search Box", ExtentColor.LIME));
		child.info("Entering by the Post that was created in thee Search Box");
		PostSearchInputField.sendKeys("Lets' Test Automation for");
		ScreenShot();
		SearchPostButton.click();
		//System.out.println("trial"); 
		//		try{
		//			NoPostFound.isDisplayed();
		//		}catch(NoSuchElementException ne){		
		//		return true;
		//		}
		//		
		//		try{
		//
		//			if(NoPostFound.isDisplayed()){
		//				child.info("No posts to delete");
		//			}else{
		//				ChkBx.click();
		//				Helper.SelectDrpDwn(ActionsDrpDwn, "trash");
		//				ApplyBtn.click();
		//			}
		//		}
		//	}catch(NoSuchElementException ne){		
		//		return ;
		//	}
		//System.out.println("tirath");

		List<WebElement> li = driver.findElements(By.xpath("//td[text()='No posts found.']"));
		//	   List<WebElement> li1 = driver.findElements((By) Nopostsfound);
		if(li.size() > 0){
			child.info("No posts to delete");}

		else
		{
			ChkBx.click();

			driver.findElement(By.xpath("//div[@id='bulk_action_selector_top_chosen']")).click();
			driver.findElement(By.xpath("//ul[@class='chosen-results']//li[text()='Move to Trash']")).click();

			// Helper.SelectDrpDwn(ActionsDrpDwn, "trash");
			ApplyBtn.click();
		}		

		//		if(NoPostFound.isDisplayed()){
		//			child.info("No posts to delete");
		//		}else{Peru@0106
		//			ChkBx.click();
		//			Helper.SelectDrpDwn(ActionsDrpDwn, "trash");
		//			ApplyBtn.click();
		//		}

		System.out.println("trial2");

		AllPostsBtn.click();
		ScreenShot();
		Helper.ExplicitWait(PostSearchInputField);
		PostSearchInputField.click();

		child.info("Searching if any more Posts are left to be deleted");
		PostSearchInputField.sendKeys("Lets' Test Automation for");
		SearchPostButton.click();

		List<WebElement> li2 = driver.findElements(By.xpath("//td[text()='No posts found.']"));
		//	   List<WebElement> li1 = driver.findElements((By) Nopostsfound);
		if(li2.size() > 0){
			child.info("No posts to delete");}

		else
		{
			ChkBx.click();
			driver.findElement(By.xpath("//div[@id='bulk_action_selector_top_chosen']")).click();
			driver.findElement(By.xpath("//ul[@class='chosen-results']//li[text()='Move to Trash']")).click();
			//Helper.SelectDrpDwn(ActionsDrpDwn, "trash");
			ApplyBtn.click();
		}



		//		if(NoPostFound.isDisplayed()){
		//			child.info("Nothing to delete");
		//		}else{
		//			ChkBx.click();
		//			Helper.SelectDrpDwn(ActionsDrpDwn, "trash");
		//			ApplyBtn.click();
		//		}


		/*Deleting the Documentation Page created */

		Helper.ExplicitWaitVisibility(DashBoard);
		child.info("Clicking on All Documentation Button");

		DocumentationBtn.click();
		AllDocumentationBtn.click();

		Helper.ExplicitWait(PostSearchInputField);
		PostSearchInputField.click();

		child.info("Entering by the Documentation that was created in the Search Box");
		PostSearchInputField.sendKeys("Automated Document Page Creation");
		ScreenShot();
		Helper.ExplicitWaitVisibility(SearchPostButton);
		SearchPostButton.click();

		List<WebElement> li3 = driver.findElements(By.xpath("//td[text()='No Documentation found']"));
		//	   List<WebElement> li1 = driver.findElements((By) Nopostsfound);
		if(li3.size() > 0){
			child.info("No Document Left");}

		else
		{
			ChkBx.click();
			//					Helper.SelectDrpDwn(ActionsDrpDwn, "trash");
			//					ScreenShot();


			driver.findElement(By.xpath("//div[@id='bulk_action_selector_top_chosen']")).click();
			driver.findElement(By.xpath("//ul[@class='chosen-results']//li[text()='Move to Trash']")).click();



			ApplyBtn.click();
			Helper.ExplicitWait(DocumentationBtn);
		}

		//		if(NoDocFound.isDisplayed()){
		//			child.info("No Document Left");
		//		}else{
		//			ChkBx.click();
		//			Helper.SelectDrpDwn(ActionsDrpDwn, "trash");
		//			ScreenShot();
		//			ApplyBtn.click();
		//			Helper.ExplicitWait(DocumentationBtn);			
		//		}
		//		ChkBx.click();
		//
		//		Helper.SelectDrpDwn(ActionsDrpDwn, "trash");
		//		ScreenShot();
		//		ApplyBtn.click();
		//
		//		Helper.ExplicitWait(DocumentationBtn);
		DocumentationBtn.click();
		Helper.ExplicitWait(AllDocumentationBtn);
		AllDocumentationBtn.click();
		ScreenShot();
		Helper.ExplicitWait(PostSearchInputField);
		PostSearchInputField.click();

		child.info("Searching by the Documentation if anything is left");
		PostSearchInputField.sendKeys("Automated Document Page Creation");
		Helper.ExplicitWaitVisibility(SearchPostButton);
		SearchPostButton.click();
		ScreenShot();

		List<WebElement> li4 = driver.findElements(By.xpath("//td[text()='No Documentation found']"));
		//	   List<WebElement> li1 = driver.findElements((By) Nopostsfound);
		if(li4.size() > 0){
			child.info("No Document Left");}

		else
		{
			ChkBx.click();
			Helper.SelectDrpDwn(ActionsDrpDwn, "trash");
			ScreenShot();
			ApplyBtn.click();
			Helper.ExplicitWait(DocumentationBtn);
		}

		//		if(NoDocFound.isDisplayed()){
		//			child.info("No Document Left to be deleted");
		//		}else{
		//			ChkBx.click();
		//			Helper.SelectDrpDwn(ActionsDrpDwn, "trash");
		//			ScreenShot();
		//			ApplyBtn.click();
		//			Helper.ExplicitWait(DocumentationBtn);			
		//		}



		/* Deleting the Categories created*/

		DocumentationBtn.click();
		DocumentationCategoriesBtn.click();		
		Helper.ExplicitWait(DocumentatioCategorySearchInputField);
		DocumentatioCategorySearchInputField.click();
		DocumentatioCategorySearchInputField.sendKeys("Father");
		SearchDocBtnBtn.click();
		ChkBx.click();	

		driver.findElement(By.xpath("//div[@id='bulk_action_selector_top_chosen']")).click();
		driver.findElement(By.xpath("//div[@id='bulk_action_selector_top_chosen']/a/span")).click();

		//Helper.SelectDrpDwn(ActionsDrpDwn, "delete");
		ApplyBtn.click();


		DocumentationBtn.click();
		DocumentationCategoriesBtn.click();		
		Helper.ExplicitWait(DocumentatioCategorySearchInputField);
		DocumentatioCategorySearchInputField.click();
		DocumentatioCategorySearchInputField.sendKeys("Child");
		SearchDocBtnBtn.click();
		ChkBx.click();	

		driver.findElement(By.xpath("//div[@id='bulk_action_selector_top_chosen']")).click();
		driver.findElement(By.xpath("//div[@id='bulk_action_selector_top_chosen']/a/span")).click();

		//Helper.SelectDrpDwn(ActionsDrpDwn, "delete");
		ApplyBtn.click();

		DocumentationBtn.click();
		DocumentationCategoriesBtn.click();		
		Helper.ExplicitWait(DocumentatioCategorySearchInputField);
		DocumentatioCategorySearchInputField.click();
		DocumentatioCategorySearchInputField.sendKeys("GrandChild");
		SearchDocBtnBtn.click();
		ChkBx.click();	

		driver.findElement(By.xpath("//div[@id='bulk_action_selector_top_chosen']")).click();
		driver.findElement(By.xpath("//div[@id='bulk_action_selector_top_chosen']/a/span")).click();

		//Helper.SelectDrpDwn(ActionsDrpDwn, "delete");
		ApplyBtn.click();

		/****Logging Out***/
		child.info("Logging out");
		Helper.moveToElement(Name);
		

		child.info("Logout btn");
		Helper.ExplicitWaitVisibility(LogOutBtn);
		LogOutBtn.click();
		ScreenShot();
		return true;
	}
}
