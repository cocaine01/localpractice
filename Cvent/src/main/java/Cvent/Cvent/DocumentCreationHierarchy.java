package Cvent.Cvent;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import Utilities.Helper;

public class DocumentCreationHierarchy extends Base{

	WebDriver driver = null;

	public DocumentCreationHierarchy(WebDriver driver){
		this.driver=driver;
	}
	@FindBy(xpath="//a[text()='Add Documentation']")
	WebElement AddDocumentationBtn;

	@FindBy(xpath="//input[@id='title']")
	WebElement DocPageTitle;

	@FindBy(xpath="//div[@class='inside']//label[@class='selectit'][text()=' Event Management']")
	WebElement EventManagementChkBox;
	//String x= "//label[@class='selectit'][text()=' Event Management']"; 

	@FindBy(xpath="//div[@class='inside']//label[@class='selectit'][text()=' Father']")
	WebElement FatherBtn;

	@FindBy(xpath="//div[@class='inside']//label[@class='selectit'][text()=' Child']")
	WebElement ChildBtn;

	@FindBy(xpath="//div[@class='inside']//label[@class='selectit'][text()=' GrandChild']")
	WebElement GrandChildBtn;

	//WebElement BaseChkBox = driver.findElement(By.xpath("//label[@class='selectit'][text()=' "+CONFIG.getProperty("parentCategory")));
	//WebElement BaseChkBox1 =driver.findElement(By.xpath("//label[@class='selectit'][text()=' {CONFIG.getProperty("parentCategory")}']"));
	//WebElement BaseChkBox = driver.findElement(By.xpath("//label[@class='selectit'][text()=' "+CONFIG.getProperty("parentCategory")+"']"));

	@FindBy(xpath="//input[@value='Update'][@type='submit']")
	WebElement UpdateBtn;

	@FindBy(xpath="//input[@id='save-post']")
	WebElement SaveDraftBtn;
	//	
	@FindBy(xpath="//input[@value='Publish'][@type='submit']")
	WebElement PublishBtn;

	@FindBy(xpath="//select[@id='parent_id']")
	WebElement PageDroprDown1;

	@FindBy(xpath="//a[text()='Preview Changes']")
	WebElement PreviewChangesBtn;	

	@FindBy(xpath="//div[text()='Documentation']")
	WebElement DocumentationBtn;

	@FindBy(xpath="//ul[@id='list-manual']")
	WebElement PagesGrid;

	@FindBy(xpath="//a[@class='ab-item'][text()='Log Out']")
	WebElement LogOutBtn;

	@FindBy(xpath="//a[@class='ab-item'][@aria-haspopup]/span[@class='display-name']")
	WebElement Name;
	
	
	@FindBy(xpath="//div[@id='parent_id_chosen']")
	WebElement ParentDocumentDropdown;
	
	@FindBy(xpath="//li[contains(text(),'Automated Document Page Creation for Event Management')]")
	WebElement FatherParentDocument;
	
	@FindBy(xpath="//li[contains(text(),'Automated Document Page Creation for Father')]")
	WebElement ChildParentDocument;
	
	@FindBy(xpath="//li[contains(text(),'Automated Document Page Creation for Child')]")
	WebElement GrandChildParentDocument;



	public boolean DocumentCategoryCreationHierarchy() throws IOException, InterruptedException{

		//******Adding the Content for Event Management****/


		child.info("Updating Content Page Wise for the Event Management");
		Helper.ExplicitWait(DocumentationBtn);
		Helper.moveToElementAndClick(DocumentationBtn);
		Helper.ExplicitWait(AddDocumentationBtn);
		AddDocumentationBtn.click();
		Helper.ExplicitWait(DocPageTitle);
		DocPageTitle.click();
		DocPageTitle.sendKeys("Automated Document Page Creation for "+CONFIG.getProperty("eventParentName"));

		StringBuffer sb = new StringBuffer();
		@SuppressWarnings("resource")
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\Data\\"+CONFIG.getProperty("eventDocumentName"));
		int data = fis.read();
		while(data!=-1){
			char c = ((char)data);
			data =fis.read();
			sb.append(c);
		}

		driver.switchTo().frame( driver.findElement(By.xpath("//iframe[@id = 'content_ifr']")));		
		WebElement Para =driver.findElement(By.xpath("//body[@id='tinymce']"));
		Para.click();
		Para.sendKeys(sb);
		child.info("Content for Event Management Created");
		ScreenShot();
		driver.switchTo().defaultContent();		
		EventManagementChkBox.click();		
		//		if(Helper.ExplicitWait(UpdateBtn)){
		//			UpdateBtn.click();
		//		}
		//		else{
		//			PublishBtn.click();
		//		}
		//Helper.SelectDrpDwnBootStrap(ParentDocument, TextContatingValue);
		Thread.sleep(5000);		
		child.info("Publishing the content for Event Management");
		PublishBtn.click();
		Helper.ExplicitWait(driver.findElement(By.xpath("//input[@id='publish'][@value='Update']")));
		Thread.sleep(5000);
		


		///************For Father*****************///

		child.info("Updating Content Page Wise for the Father");
		System.out.println("gvsvscjhsvcjhs");
		Helper.ExplicitWait(DocumentationBtn);
		System.out.println("gvsvscjhsvcjhs");
		Helper.moveToElementAndClick(DocumentationBtn);
		System.out.println("gvsvscjhsvcjhs");
		Helper.ExplicitWait(AddDocumentationBtn);
		AddDocumentationBtn.click();
		Helper.ExplicitWait(DocPageTitle);
		DocPageTitle.click();
		DocPageTitle.sendKeys("Automated Document Page Creation for "+CONFIG.getProperty("fatherName"));

		StringBuffer sb1 = new StringBuffer();
		@SuppressWarnings("resource")
		FileInputStream fis1 = new FileInputStream(System.getProperty("user.dir")+"\\Data\\"+CONFIG.getProperty("fatherDocumentName"));
		int data1 = fis1.read();
		while(data1!=-1){
			char c1 = ((char)data1);
			data1 =fis1.read();
			sb1.append(c1);			
		}

		driver.switchTo().frame( driver.findElement(By.xpath("//iframe[@id = 'content_ifr']")));		
		WebElement Para1 =driver.findElement(By.xpath("//body[@id='tinymce']"));
		Para1.click();
		Para1.sendKeys(sb1);
		child.info("Content for Father Created");
		ScreenShot();
		driver.switchTo().defaultContent();		

		FatherBtn.click();
		Helper.SelectDrpDwnBootStrap(ParentDocumentDropdown, FatherParentDocument);
	//	Helper.SelectDrpDwnText(PageDroprDown1, "Automated Document Page Creation for Event Management");
		child.info("Publishing the content for Father Category");
		PublishBtn.click();
		Thread.sleep(5000);

		///************For Child*****************///

		child.info("Updating Content Page Wise for the Child");
		DocumentationBtn.click();
		AddDocumentationBtn.click();
		Helper.ExplicitWait(DocPageTitle);
		DocPageTitle.click();
		DocPageTitle.sendKeys("Automated Document Page Creation for "+CONFIG.getProperty("childName"));

		StringBuffer sb2 = new StringBuffer();
		@SuppressWarnings("resource")
		FileInputStream fis2 = new FileInputStream(System.getProperty("user.dir")+"\\Data\\"+CONFIG.getProperty("childDocumentName"));
		int data2 = fis2.read();
		while(data2!=-1){
			char c2 = ((char)data2);
			data2 =fis2.read();
			sb2.append(c2);			
		}

		driver.switchTo().frame( driver.findElement(By.xpath("//iframe[@id = 'content_ifr']")));		
		WebElement Para2 =driver.findElement(By.xpath("//body[@id='tinymce']"));
		Para2.click();
		Para2.sendKeys(sb2);
		child.info("Content for Child Created");
		ScreenShot();
		driver.switchTo().defaultContent();	


		ChildBtn.click();
		Helper.SelectDrpDwnBootStrap(ParentDocumentDropdown, ChildParentDocument);
		//Helper.SelectDrpDwnTextWithSpaceForFirstChild(PageDroprDown1,"Automated Document Page Creation for Father");
		child.info("Publishing the content for Child Category");
		((JavascriptExecutor)driver).executeScript("scroll(0,-400)");

		PublishBtn.click();
		Thread.sleep(5000);

		///************For Grand Child*****************///

		child.info("Updating Content Page Wise for the Grand Child");
		DocumentationBtn.click();
		AddDocumentationBtn.click();
		Helper.ExplicitWait(DocPageTitle);
		DocPageTitle.click();
		DocPageTitle.sendKeys("Automated Document Page Creation for "+CONFIG.getProperty("grandChildName"));

		StringBuffer sb3 = new StringBuffer();
		FileInputStream fis3 = new FileInputStream(System.getProperty("user.dir")+"\\Data\\"+CONFIG.getProperty("grandChildDocumentName"));
		int data3 = fis3.read();
		while(data3!=-1){
			char c3 = ((char)data3);
			data3 =fis3.read();
			sb3.append(c3);			
		}

		driver.switchTo().frame( driver.findElement(By.xpath("//iframe[@id = 'content_ifr']")));		
		WebElement Para3 =driver.findElement(By.xpath("//body[@id='tinymce']"));
		Para3.click();
		Para3.sendKeys(sb3);
		child.info("Content for Grand Child Created");
		ScreenShot();
		driver.switchTo().defaultContent();	


		GrandChildBtn.click();
		Helper.SelectDrpDwnBootStrap(ParentDocumentDropdown, GrandChildParentDocument);
		//Helper.SelectDrpDwnTextWithSpaceForSecondChild(PageDroprDown1,"Automated Document Page Creation for Child");
		((JavascriptExecutor)driver).executeScript("scroll(0,-400)");
		child.info("Publishing the content for Grand Child Category");
		PublishBtn.click();

		String parentWindow =driver.getWindowHandle();
		System.out.println("Parent window" +parentWindow);

		child.info("Clicking on Preview Changes Button");
		PreviewChangesBtn.click();

		Set <String> allWindows =driver.getWindowHandles();		     
		int count =allWindows.size();

		for(String child  : allWindows){
			if (!parentWindow.equalsIgnoreCase(child)){
				driver.switchTo().window(child);

				driver.findElement(By.xpath("//li[@id='menu-item-136']/a[text()='Documentation']")).click();

				driver.findElement(By.xpath("//li[@id='menu-item-133']/a[text()='Event Management']")).click();

				Actions action = new Actions(driver);

				action.moveToElement(PagesGrid).click().build().perform();
				action.sendKeys(Keys.PAGE_DOWN).build().perform();
				//		    	        driver.findElement(By.xpath("//a[@href='http://apidev.cvent.com/index.php/documentation/automated-document-page-creation-for-event-management/']")).click();
				//		    	        action.sendKeys(Keys.PAGE_DOWN).build().perform();
				//		    	        driver.findElement(By.xpath("http://apidev.cvent.com/index.php/documentation/automated-document-page-creation-for-event-management/automated-document-page-creation-for-father/")).click();
				//		    	        action.sendKeys(Keys.PAGE_DOWN).build().perform();
				//		    	        driver.findElement(By.xpath("http://apidev.cvent.com/index.php/documentation/automated-document-page-creation-for-event-management/automated-document-page-creation-for-father/automated-document-page-creation-for-child/")).click();
			}
		}
		ScreenShot();


		driver.switchTo().window(parentWindow);
		/*Logging Out Process*/
		child.info("Logging out");
		Helper.moveToElement(Name);

		child.info("Logout btn");
		Helper.ExplicitWaitVisibility(LogOutBtn);
		LogOutBtn.click();
		ScreenShot();



		return true;
	}

}

