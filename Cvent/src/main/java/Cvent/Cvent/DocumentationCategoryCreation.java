package Cvent.Cvent;

import java.io.FileInputStream;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import Utilities.Helper;



public class DocumentationCategoryCreation extends Base{
	WebDriver driver = null;

	@FindBy(xpath="//div[text()='Documentation']")
	WebElement DocumentationBtn;

	@FindBy(xpath="//a[text()='Documentation Categories']")
	WebElement DocumentationCategoriesBtn;

	@FindBy(xpath="//input[@id='tag-name']")
	WebElement NameField;

	@FindBy(xpath="//div[@id='parent_chosen']")
	WebElement ParentCategoryDropDown;

	@FindBy(xpath="//input[@value='Add New Category']")
	WebElement AddNewCategoryBtn;

	@FindBy(xpath="//input[@id='tag-search-input']")
	WebElement SearchCategoriesBox;

	//---Content Addition---//

	@FindBy(xpath="//a[text()='Add Documentation']")
	WebElement AddDocumentationBtn;

	@FindBy(xpath="//input[@id='title']")
	WebElement DocPageTitle;

	@FindBy(xpath="//div[@class='inside']//label[@class='selectit'][text()=' Event Management']")
	WebElement EventManagementChkBox;
	
	
	
//	
//	final String str4 = "//div[@class='inside']/label[@class='selectit'][text()='" + CONFIG.getProperty("eventParentName") + "']" ;
//	 
//	@FindBy(how = How.XPATH, using = str4)
//	   public WebElement EventManagementChkBox1;
	
	

	
	//String x= "//label[@class='selectit'][text()=' Event Management']"; 

	@FindBy(xpath="//div[@class='inside']//label[@class='selectit'][text()=' Father']")
	WebElement FatherBtn;

	@FindBy(xpath="//div[@class='inside']//label[@class='selectit'][text()=' Child']")
	WebElement ChildBtn;

	@FindBy(xpath="//div[@class='inside']//label[@class='selectit'][text()=' GrandChild']")
	WebElement GrandChildBtn;



	//WebElement BaseChkBox = driver.findElement(By.xpath("//label[@class='selectit'][text()=' "+CONFIG.getProperty("parentCategory")));
	//WebElement BaseChkBox1 =   driver.findElement(By.xpath("//label[@class='selectit'][text()=' {CONFIG.getProperty("parentCategory")}']"));
	//WebElement BaseChkBox =   driver.findElement(By.xpath("//label[@class='selectit'][text()=' "+CONFIG.getProperty("parentCategory")+"']"));



	@FindBy(xpath="//input[@value='Update'][@type='submit']")
	WebElement UpdateBtn;

	@FindBy(xpath="//input[@id='save-post']")
	WebElement SaveDraftBtn;
	//	
	@FindBy(xpath="//input[@value='Publish'][@type='submit']")
	WebElement PublishBtn;



		@FindBy(xpath="//select[@id='parent_id']")
		WebElement PageDroprDown1;
	//	
		@FindBy(xpath="//li[contains(text(),'Event Management')]")
		WebElement EventManagementDropdown;
		
		@FindBy(xpath="//li[contains(text(),'Father')]")
		WebElement FatherDropdown;
		
		@FindBy(xpath="//li[contains(text(),'Child')]")
		WebElement ChildCategory;
		
	
		
		

	public DocumentationCategoryCreation(WebDriver driver){
		this.driver=driver;
	}


	@SuppressWarnings("resource")
	public boolean DocumentCategoryCreation() throws IOException{
		child.info("Clicking on the 'Documentation' button");
		ScreenShot();
		Helper.ExplicitWait(DocumentationBtn);
		DocumentationBtn.click();
		Helper.ExplicitWait(DocumentationCategoriesBtn);
		child.info("Clicking on the 'Documentation Categories' button");
		DocumentationCategoriesBtn.click();
		Helper.ExplicitWait(NameField);
		NameField.click();
		child.info("Entering the Category Name");
		//NameField.sendKeys(CONFIG.getProperty("fatherName"));
		Helper.highLightEleAndSendKeys(NameField, CONFIG.getProperty("fatherName"));		
		//Helper.SelectDrpDwnText(ParentCategoryDropDown, CONFIG.getProperty("parentCategory"));
        Helper.SelectDrpDwnBootStrap(ParentCategoryDropDown , EventManagementDropdown);
		Helper.ExplicitWait(AddNewCategoryBtn);
		ScreenShot();
		AddNewCategoryBtn.click();



		child.info("Adding new Child Category:::Clicking on the 'Documentation' button");
		ScreenShot();
		Helper.ExplicitWait(DocumentationBtn);
		Helper.moveToElementAndClick(DocumentationBtn);
		DocumentationBtn.click();
		child.info("Clicking on the 'Documentation Categories ' button");
		DocumentationCategoriesBtn.click();
		Helper.ExplicitWait(NameField);
		NameField.click();
		//NameField.sendKeys(CONFIG.getProperty("childName"));
		Helper.highLightEleAndSendKeys(NameField, CONFIG.getProperty("childName"));		
		Helper.SelectDrpDwnBootStrap(ParentCategoryDropDown , FatherDropdown);
		//Helper.SelectDrpDwnTextWithSpaceForFirstChild(ParentCategoryDropDown, CONFIG.getProperty("childParentCategory"));		
		Helper.ExplicitWait(AddNewCategoryBtn);
		ScreenShot();
		AddNewCategoryBtn.click();


		child.info("Adding new GrandChild Category:::Clicking on the 'Documentation' button");
		ScreenShot();
		Helper.ExplicitWait(DocumentationBtn);
		Helper.moveToElementAndClick(DocumentationBtn);
		DocumentationBtn.click();
		child.info("Clicking on the 'Documentation Categories ' button");
		DocumentationCategoriesBtn.click();
		Helper.ExplicitWait(NameField);
		NameField.click();	
		Helper.highLightEleAndSendKeys(NameField, CONFIG.getProperty("grandChildName"));		
		Helper.SelectDrpDwnBootStrap(ParentCategoryDropDown , ChildCategory);
		//Helper.SelectDrpDwnTextWithSpaceForSecondChild(ParentCategoryDropDown, CONFIG.getProperty("grandChildParentCategory"));
		AddNewCategoryBtn.click();
		//				Helper.ExplicitWait(SearchCategoriesBox);
		//				SearchCategoriesBox.click();



		return true;

	}
}
