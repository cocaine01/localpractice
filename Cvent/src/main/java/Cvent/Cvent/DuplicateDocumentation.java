package Cvent.Cvent;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Utilities.Helper;

public class DuplicateDocumentation extends Base{

	@FindBy(xpath="//div[text()='Documentation']")
	WebElement DocumentationBtn;
	
	@FindBy(xpath="//a[text()='All Documentation']")
	WebElement AllDocumentationBtn;
	
	@FindBy(xpath="//input[@id='post-search-input']")
	WebElement PostSearchInputField;
	
	@FindBy(xpath="")
	WebElement x;
	
	
	@FindBy(xpath="//a[text()='Add Documentation']")
	WebElement AddDocumentationBtn;

	@FindBy(xpath="//input[@id='title']")
	WebElement DocPageTitle;
	@FindBy(xpath="//div[@class='inside']//label[@class='selectit'][text()=' Event Management']")
	WebElement EventManagementChkBox;
	@FindBy(xpath="//input[@value='Publish'][@type='submit']")
	WebElement PublishBtn;
	@FindBy(xpath="//a[text()='Preview Changes']")
	WebElement PreviewChangesBtn;
	@FindBy(xpath="//div[@id='duplicate-action']/a")
	WebElement DuplicateAndEdit;	
	@FindBy(xpath="//input[@name='merge_back']")
	WebElement MergeBacToOriginalPostBtn;	
	@FindBy(xpath="//a[@class='ab-item'][text()='Log Out']")
	WebElement LogOutBtn;
	@FindBy(xpath="//a[@class='ab-item'][@aria-haspopup]/span[@class='display-name']")
	WebElement Name;
	
	WebDriver driver = null ;
	public DuplicateDocumentation(WebDriver driver) {
		this.driver=driver;
	}
	public boolean TestDuplicatePostMtd() throws IOException, InterruptedException{
		child.info("Clicking on All Documentation Button");
		Helper.ExplicitWaitVisibility(DocumentationBtn);
		DocumentationBtn.click();		
		ScreenShot();
		AddDocumentationBtn.click();
		Helper.ExplicitWait(DocPageTitle);
		DocPageTitle.click();
		DocPageTitle.sendKeys("Automated Document Page Creation for Version control feature");

		StringBuffer sb = new StringBuffer();
		@SuppressWarnings("resource")
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\Data\\"+CONFIG.getProperty("eventDocumentName"));
		int data = fis.read();
		while(data!=-1){
			char c = ((char)data);
			data =fis.read();
			sb.append(c);			
		}
		driver.switchTo().frame( driver.findElement(By.xpath("//iframe[@id = 'content_ifr']")));		
		WebElement Para =driver.findElement(By.xpath("//body[@id='tinymce']"));
		Para.click();
		Para.sendKeys(sb);
		child.info("Content for Version control feature Created");
		ScreenShot();
		driver.switchTo().defaultContent();		
		EventManagementChkBox.click();
		
		child.info("Publishing the content for Version control feature");
		PublishBtn.click();
		
		child.info("Clicking on Preview Changes Button");
	    PreviewChangesBtn.click();
	    
	    String parentWindow =driver.getWindowHandle();
	    Set <String> allWindows =driver.getWindowHandles();		     
	     int count =allWindows.size();
	    
	    for(String child  : allWindows){
	    	 if (!parentWindow.equalsIgnoreCase(child)){
	    		 driver.switchTo().window(child);
	    		 driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
	    		 ScreenShot();
	    	 }  	    	 
	    	 }
		 driver.switchTo().window(parentWindow);
		 ScreenShot();
		 child.info("Clicking on Duplicate and Edit Button");
		 DuplicateAndEdit.click();
		ScreenShot();
		Helper.ExplicitWait(DocPageTitle);
		DocPageTitle.click();
		child.info("Validating if it is the correct duplicate Documentation");
		System.out.println("tirath");
		String [] docTitleArr = DocPageTitle.getAttribute("value").split(" ");
		System.out.println(docTitleArr);
		Assert.assertEquals(docTitleArr[0],"[Duplicated]");
		child.info("Verified");
//		DocPageTitle.clear();
//		
//		child.info("Entering new Title");
//		DocPageTitle.click();
//		DocPageTitle.sendKeys("Version control V1.1. Automated Testing Document for Version control ");
		
		StringBuffer sb1 = new StringBuffer();
		@SuppressWarnings("resource")
		FileInputStream fis1 = new FileInputStream(System.getProperty("user.dir")+"\\Data\\"+CONFIG.getProperty("duplicateDocumentName"));
		int data1 = fis1.read();
		while(data1!=-1){
			char c1 = ((char)data1);
			data1 =fis1.read();
			sb1.append(c1);			
		}
		driver.switchTo().frame( driver.findElement(By.xpath("//iframe[@id = 'content_ifr']")));		
		WebElement Para1 =driver.findElement(By.xpath("//body[@id='tinymce']"));
		
		Para1.click();
		Para1.clear();
		Para1.sendKeys(sb1);
		
		child.info("New content Entered");
		ScreenShot();
		driver.switchTo().defaultContent();	
		ScreenShot();
		MergeBacToOriginalPostBtn.click();
		
		
		//driver.switchTo().alert().accept();
		
		//new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.AlertIsPresent()).Accept();
//		//		( ( JavascriptExecutor ) driver )
//        .executeScript( "window.onbeforeunload = function(e){};" );
		
		/*Handling Browser Pop Up */
		//System.out.println("tirath raftar");
		Alert alert= driver.switchTo().alert();
		alert.accept();
		
		
//		Actions builder= new Actions(driver);
//		builder.keyDown(Keys.ENTER).build().perform();
//		Thread.sleep(3000);
//		builder.keyUp(Keys.ENTER).build().perform();
		
//		try{
//			Alert alert= driver.switchTo().alert();
//			alert.accept();
//			
//		}catch(Exception e){
//			System.out.println("org.openqa.selenium.UnhandledAlertException: unexpected alert open: {Alert text : } ");
//			Runtime.getRuntime().exec("C:\\Users\\sumeetk\\Desktop\\CEvent\\AutoIT\\duplicateDocumentation.exe");
//		}		
				//Runtime.getRuntime().exec(System.getProperty("user.dir")+"\\AutoItScripts\\duplicateDocumentation.exe");
		                         
		ScreenShot();	
		child.info("Clicking on Preview Changes Button");
	    PreviewChangesBtn.click();
	    
	    String parentWindow1 =driver.getWindowHandle();
	    Set <String> allWindows1 =driver.getWindowHandles();		     
	     int count1 =allWindows1.size();
	    
	    for(String child1  : allWindows1){
	    	 if (!parentWindow1.equalsIgnoreCase(child1)){
	    		 driver.switchTo().window(child1);
	    		 driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
	    		 ScreenShot();
	    	 }  	    	 
	    	 }
	    driver.switchTo().window(parentWindow1);
	    
	    /*Logging Out Process*/
	    child.info("Logging out");
		Helper.moveToElement(Name);
		
		child.info("Logout btn");
		Helper.ExplicitWaitVisibility(LogOutBtn);
		LogOutBtn.click();
        ScreenShot();	
		
		
		 
		
		return true;
	}
	
	
	
}
