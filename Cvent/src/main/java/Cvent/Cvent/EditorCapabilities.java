package Cvent.Cvent;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import Utilities.Helper;

public class EditorCapabilities extends Base{
	
	WebDriver driver = null;
	
//	@FindBy(xpath="//input[@id='save-post']")
//	WebElement SaveDraftBtn;
	
	@FindBy(xpath="//div[text()='Posts']")
	WebElement PostsButton;
	
	
	@FindBy(xpath="//a[text()='All Posts']")
	WebElement AllPostsBtn;	
	
	@FindBy(xpath="//input[@id='post-search-input']")
	WebElement PostSearchInputField;
	
	@FindBy(xpath="//input[@id='search-submit']")
	WebElement SearchPostButton ;
	
	@FindBy(xpath="//a[text()='Edit']")
	WebElement EditBtn;
	
	@FindBy(xpath="//input[@id='title']")
	WebElement Title;
	
	@FindBy(xpath="//a[@class='edit-post-status hide-if-no-js']")
	WebElement StatusEditBtn;
	@FindBy(xpath="//select[@id='post_status']")
	WebElement StatusDrpDwn;
	
	@FindBy(xpath="//a[text()='OK'][@class='save-post-status hide-if-no-js button']")
	WebElement OKBtn;
	
	@FindBy(xpath="//input[@value='Save as Pending']")
	WebElement SaveAsPendingBtn;
	
	@FindBy(xpath="//input[@value='Save as Reviewed']")
	WebElement SaveAsReviewedBtn;
	//
	@FindBy(xpath="//tbody[@id='the-list']/tr")
	WebElement MouseHoverElement;
	
	@FindBy(xpath="//a[@class='ab-item'][@aria-haspopup]/span[@class='display-name']")
	WebElement Name;
	
	@FindBy(xpath="//a[@class='ab-item'][text()='Log Out']")
	WebElement LogOutBtn;
	
//	@FindBy(xpath="//tbody[@id='the-list']")
//	WebElement ;
	
//	@FindBy(xpath="")
//	WebElement ;
	
	public EditorCapabilities(WebDriver driver){
		this.driver=driver;
	}
	
	public boolean EditorCapabilitiesMtd(String heading) throws IOException{
		child.info("Clicking on All Posts Button");
		
		Helper.ExplicitWaitVisibility(PostsButton);
		Helper.highLightEleAndClick(PostsButton);
		//PostsButton.click();
		Helper.highLightEleAndClick(AllPostsBtn);
		//AllPostsBtn.click();
		ExtendedScreenShot();
		Helper.ExplicitWait(PostSearchInputField);
		Helper.highLightEleAndClick(PostSearchInputField);
		//PostSearchInputField.click();
		
		child.info(MarkupHelper.createLabel("Searching by the Post that was created in the Search Box", ExtentColor.CYAN));
		Helper.highLightEleAndSendKeys(PostSearchInputField, "Lets' Test Automation for "+heading);
		ScreenShot();
		child.info("Clicking on Search Post Button");
		Helper.highLightEleAndClick(SearchPostButton);
		//SearchPostButton.click();
		
		MouseHoverElement.click();
		
		ScreenShot();
		child.log(Status.PASS, MarkupHelper.createLabel("Clicking on Edit Post", ExtentColor.CYAN));
		//child.info("Clicking on Edit Post");
		Helper.ExplicitWait(EditBtn);
		Helper.highLightEleAndClick(EditBtn);
		//EditBtn.click();
		Helper.ExplicitWait(Title);
		ScreenShot();
		
		child.info(MarkupHelper.createLabel("Changing the Status Now to Pending review", ExtentColor.CYAN));
		StatusEditBtn.click();
		ScreenShot();
		child.info("Selecting from the dropdown");
		Helper.SelectDrpDwn(StatusDrpDwn, "pending");
		Helper.highLightEleAndClick(OKBtn);
        //OKBtn.click();
        ScreenShot();
        child.info("Clicking on 'Save as Pending Review' button");
        Helper.ExplicitWait(SaveAsPendingBtn);
        SaveAsPendingBtn.click();          
        ScreenShot();
		
        Helper.highLightEleAndClick(AllPostsBtn);
        //AllPostsBtn.click();
        Helper.ExplicitWait(PostSearchInputField);
        Helper.highLightEleAndClick(PostSearchInputField);
		//PostSearchInputField.click();
        child.info("Searching the previous post");
		Helper.highLightEleAndSendKeys(PostSearchInputField, "Lets' Test Automation for "+heading);
		Helper.highLightEleAndClick(SearchPostButton);
		//SearchPostButton.click();
		
		child.info(MarkupHelper.createLabel("New Status of the Post is now 'Pending'", ExtentColor.CYAN));
	   	ScreenShot();
		//////////////////////////////////////
		child.info("Searching by the Post that was created in the Search Box");
		PostSearchInputField.clear();
		child.info("Entering the  Subject Line to search for");
		Helper.highLightEleAndSendKeys(PostSearchInputField, "Lets' Test Automation for "+heading);
		ScreenShot();
		child.info("Clicking on 'Search Post' Button");
		SearchPostButton.click();
		
		MouseHoverElement.click();		
		ScreenShot();
		
		child.info("Clicking on Edit Post");
		Helper.ExplicitWait(EditBtn);
		EditBtn.click();
		Helper.ExplicitWait(Title);
		ScreenShot();
		child.info(MarkupHelper.createLabel("Changing the Status Now to 'Reviewed' review", ExtentColor.CYAN));
		StatusEditBtn.click();
		ScreenShot();
		Helper.SelectDrpDwn(StatusDrpDwn, "reviewed");
		Helper.highLightEleAndClick(OKBtn);
        //OKBtn.click();
        
        Helper.ExplicitWait(SaveAsReviewedBtn);
        Helper.highLightEleAndClick(SaveAsReviewedBtn);
        //SaveAsReviewedBtn.click();           
        ScreenShot();
		
        AllPostsBtn.click();
        Helper.ExplicitWait(PostSearchInputField);
        Helper.highLightEleAndClick(PostSearchInputField);
		//PostSearchInputField.click();
		Helper.highLightEleAndSendKeys(PostSearchInputField, "Lets' Test Automation for "+heading);
		Helper.highLightEleAndClick(SearchPostButton);
		//SearchPostButton.click();	
		
		child.info(MarkupHelper.createLabel("No Status of the Post means its been reviewed", ExtentColor.CYAN));
		
		/*Looging out***/
		child.info("Logging out");
		Helper.moveToElement(Name);
		ScreenShot();
		//Helper.moveToElementJS(Name);
		//Helper.moveToElement(PostsButton);
		//Helper.moveToElement(Name);
        
        child.info("Logging out");
		Helper.moveToElement(Name);
		Helper.ExplicitWaitVisibility(LogOutBtn);
		child.info(MarkupHelper.createLabel("Logout button Clicked", ExtentColor.LIME));
		
		LogOutBtn.click();
		ScreenShot();
		
		return true;
	}
	

}
