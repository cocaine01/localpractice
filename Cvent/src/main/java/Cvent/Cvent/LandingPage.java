package Cvent.Cvent;


import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import Utilities.Helper;

public class LandingPage extends Base {
	WebDriver driver = null;
	
	@FindBy(xpath="//a[contains(@class,'menu-icon-users')]")
	WebElement UsersBtn ;
	
	@FindBy(xpath="//a[@href='http://apidev.cvent.com/wp-admin/user-new.php'][text()='Add New']")
	WebElement AddNewBtn;
	
	@FindBy(xpath="//input[@id='user_login']")
	WebElement username;
	
	@FindBy(xpath="//input[@id='email']")
	WebElement email_id;
	
	@FindBy(xpath="//input[@id='first_name']")
	WebElement first_name;
	
	@FindBy(xpath="//input[@id='last_name']")
	WebElement last_name;
	
	@FindBy(xpath="//button[text()='Show password']")
	WebElement ShowPassword;
	
	@FindBy(xpath="//input[@id='pass1-text']")
	WebElement PasswordField ;
	
	@FindBy(xpath="//input[@id='send_user_notification']")
	WebElement chkBox;
//	
	@FindBy(xpath="//select[@id='role']")
	WebElement drpdwn;
	
    public LandingPage(WebDriver driver){
    	//PageFactory.initElements(driver, this);
    	this.driver = driver;
    }
    
    public void CreateUserMtd() throws IOException{
    	UsersBtn.click();
    	child.info(" Users Button Clicked");
    	Base.ScreenShot();
    	child.info(" Clicking on Add New Button");
    	AddNewBtn.click();
    	child.info(" Form Opens and so enter the credentials");
    	
    	Base.ScreenShot();
    	username.sendKeys(CONFIG.getProperty("username"));
    	email_id.sendKeys(CONFIG.getProperty("email_id"));
    	first_name.sendKeys(CONFIG.getProperty("first_name"));
    	last_name.sendKeys(CONFIG.getProperty("last_name"));
    	ShowPassword.click();  
    	Helper.ExplicitWait(PasswordField);
    	PasswordField.clear(); 
    	PasswordField.sendKeys(CONFIG.getProperty("PasswordField"));
    	Helper.SelectDrpDwn(drpdwn, "contributor");
    	Helper.Checkbox(chkBox);
    	Base.ScreenShot();
    	//return new PostCreation(driver);   
    	
    	//driver.findElement(By.name("createuser")).click();
    	
    	//return CreateUserMtd();
    }
}
