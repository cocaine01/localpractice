package Cvent.Cvent;

import java.io.IOException;
import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import Utilities.Helper;

public class PostCreation extends Base {

	WebDriver driver = null;
	
	@FindBy(xpath="//div[text()='Posts']")
	WebElement PostsButton;
	
	@FindBy(xpath="//a[text()='Add New'][@class='page-title-action']")
	WebElement addNewBtn;
	
	@FindBy(xpath="//input[@type='text'][@name='post_title']")
	WebElement postTitleField;
	
	@FindBy(xpath="//body[@data-id='content']")
	WebElement ContentPage;
	
	//Status Selection
	@FindBy(xpath="//input[@id='save-post']")
	WebElement SaveDraftBtn;
	
	@FindBy(xpath="//a[@id='post-preview']")
	WebElement PreviewBtn;
	
	@FindBy(xpath="//button[@id='insert-media-button']")
	WebElement AddMediaBtn;
	
	
	@FindBy(xpath="//button[@id='__wp-uploader-id-1']")
	WebElement SelectFileBtn;
	
	@FindBy(xpath="//button[text()='Insert into post']")
	WebElement INsertIntoPOstBtn;
	
	@FindBy(xpath="//a[text()='Upload Files']")
	WebElement UploadFilesLink;
//	
//	@FindBy(xpath="")
//	WebElement ;
	
//	@FindBy(xpath="")
//	WebElement ;
//	
  
	
	public PostCreation(WebDriver driver){
		//PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	public boolean PostCreationMtd() throws IOException{
		
		child.info("Clicking on Posts Button");
		PostsButton.click();
		ScreenShot();
		child.info("Clicking on Add New Button");		
		addNewBtn.click();
		Helper.ExplicitWait(postTitleField);
		ScreenShot();
		
		postTitleField.sendKeys("Test Automation");
		
		String parentHandle = driver.getWindowHandle(); // get the current window handle
		child.info("Clicking on Add Media Button Button");
		AddMediaBtn.click(); // click some link that opens a new window
		ScreenShot();

		for (String winHandle : driver.getWindowHandles()) {
		    driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		}
		
		UploadFilesLink.click();
		//code to do something on new window
		SelectFileBtn.click();
		ScreenShot();
		
		//AutoIT Implementation
		try {
			Runtime.getRuntime().exec("C:\\Users\\sumeetk\\Desktop\\CEvent\\FileUpload.exe");
		} catch (IOException e) {
			e.printStackTrace();
		}
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
	    driver.switchTo().window(tabs2.get(1));
		
		Helper.ExplicitWait(INsertIntoPOstBtn);
		INsertIntoPOstBtn.click();
		//driver.close(); // close newly opened window when done with it
		driver.switchTo().window(parentHandle); 
		ScreenShot();
		
		SaveDraftBtn.click();
		
		return true;
		
	    
		
		
		
		
		
	}
	
}
