package Cvent.Cvent;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import Utilities.Helper;

public class TechWriterCapabilities extends Base{
	
	WebDriver driver = null;
	
	@FindBy(xpath="//button[@value='draft']")
	WebElement SaveDraftBtn;
	
	@FindBy(xpath="//div[text()='Posts']")
	WebElement PostsButton;
		
	@FindBy(xpath="//a[text()='All Posts']")
	WebElement AllPostsBtn;	
	
	@FindBy(xpath="//input[@id='post-search-input']")
	WebElement PostSearchInputField;
	
	@FindBy(xpath="//input[@id='search-submit']")
	WebElement SearchPostButton ;
	
	@FindBy(xpath="//a[text()='Edit']")
	WebElement EditBtn;
	
	@FindBy(xpath="//input[@id='title']")
	WebElement Title;
	
	@FindBy(xpath="//a[@class='edit-post-status hide-if-no-js']")
	WebElement StatusEditBtn;
	@FindBy(xpath="//select[@id='post_status']")
	WebElement StatusDrpDwn;
	
	@FindBy(xpath="//a[text()='OK'][@class='save-post-status hide-if-no-js button']")
	WebElement OKBtn;
	
	@FindBy(xpath="//input[@value='Save as Pending']")
	WebElement SaveAsPendingBtn;
	
	@FindBy(xpath="//input[@value='Save as Reviewed']")
	WebElement SaveAsReviewedBtn;
	//
	@FindBy(xpath="//tbody[@id='the-list']/tr")
	WebElement MouseHoverElement;
	
	@FindBy(xpath="//a[@class='ab-item'][@aria-haspopup]/span[@class='display-name']")
	WebElement Name;
	
	@FindBy(xpath="//a[@class='ab-item'][text()='Log Out']")
	WebElement LogOutBtn;
	
	//New Changes for Tech Writer
	
	@FindBy(xpath="//input[@type='submit' and @id='publish']")
	WebElement SubmitForReviewBtn;
	
	@FindBy(xpath="//span[text()='Pending Review']")
	WebElement PostStatusPR;
	
	@FindBy(xpath="//span[text()='Draft']")
	WebElement PostStatusDraft;
	
	
	public TechWriterCapabilities(WebDriver driver){
		this.driver=driver;
	}
	
	public boolean TechWriterCapabilitiesMtd(String heading) throws IOException{
		child.info("Clicking on All Posts Button");
		
		Helper.ExplicitWaitVisibility(PostsButton);
		Helper.highLightEleAndClick(PostsButton);
		Helper.highLightEleAndClick(AllPostsBtn);
		ExtendedScreenShot();
		Helper.ExplicitWait(PostSearchInputField);
		Helper.highLightEleAndClick(PostSearchInputField);
				
		child.info(MarkupHelper.createLabel("Searching by the Post that was created in the Search Box", ExtentColor.CYAN));
		Helper.highLightEleAndSendKeys(PostSearchInputField, "Lets' Test Automation for "+heading);
		ScreenShot();
		child.info("Clicking on Search Post Button");
		Helper.highLightEleAndClick(SearchPostButton);
			
		MouseHoverElement.click();
		
		ScreenShot();
		child.log(Status.PASS, MarkupHelper.createLabel("Clicking on Edit Post", ExtentColor.CYAN));
		Helper.ExplicitWait(EditBtn);
		Helper.highLightEleAndClick(EditBtn);
		Helper.ExplicitWait(Title);
		ScreenShot();
		
		//*Changes for Tech Writer//
		child.info(MarkupHelper.createLabel("Now Submitting the Post for Review, Clicking on 'Submit for Review' button'", ExtentColor.CYAN));
		Helper.ExplicitWait(SubmitForReviewBtn);
		Helper.highLightEleAndClick(SubmitForReviewBtn);
		ScreenShot();
		
		child.info("Veriffying that the status is Pending Review displayed in the right hand side window");
        Assert.assertEquals(PostStatusPR.isDisplayed(), PostStatusPR.isDisplayed());	
        ScreenShot();
        
        child.info(MarkupHelper.createLabel("", ExtentColor.CYAN));
        child.info(MarkupHelper.createLabel("Now Changing the Status back to Draft", ExtentColor.CYAN));
  
        Helper.ExplicitWait(SaveDraftBtn);
		Helper.highLightEleAndClick(SaveDraftBtn);
		
		 Helper.ExplicitWait(SaveDraftBtn);
		
		child.info("Veriffying that the status is now Draft , displayed in the right hand side window");
        Assert.assertEquals(PostStatusDraft.isDisplayed(), PostStatusDraft.isDisplayed());	
        ScreenShot();
        
        child.info(MarkupHelper.createLabel("Now Changing teh Status back to Pending Review", ExtentColor.CYAN));
        Helper.ExplicitWait(SubmitForReviewBtn);
		Helper.highLightEleAndClick(SubmitForReviewBtn);
		child.info("Veriffying that the status is now again Pending Review displayed in the right hand side window");
        Assert.assertEquals(PostStatusPR.isDisplayed(), PostStatusPR.isDisplayed());
		ScreenShot();
		
		child.info(MarkupHelper.createLabel("Checking teh staus on all posts screen as well", ExtentColor.CYAN));
		
		Helper.highLightEleAndClick(AllPostsBtn);
        Helper.ExplicitWait(PostSearchInputField);
        Helper.highLightEleAndClick(PostSearchInputField);
		child.info("Searching the previous post");
		Helper.highLightEleAndSendKeys(PostSearchInputField, "Lets' Test Automation for "+heading);
		Helper.highLightEleAndClick(SearchPostButton);
		child.info(MarkupHelper.createLabel("New Status of the Post is now 'Pending'", ExtentColor.CYAN));
	   	ScreenShot();
		
	   //	Logging out**
		child.info("Logging out");
		Helper.moveToElement(Name);
		ScreenShot();
		child.info("Logging out");
		Helper.moveToElement(Name);
		Helper.ExplicitWaitVisibility(LogOutBtn);
		child.info(MarkupHelper.createLabel("Logout button Clicked", ExtentColor.LIME));
		
		LogOutBtn.click();
		ScreenShot();
		return true;
	}
	

}
