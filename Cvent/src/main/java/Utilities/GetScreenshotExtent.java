package Utilities;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import Cvent.Cvent.Base;


public class GetScreenshotExtent extends Base{
	
	public static String capture(WebDriver driver) throws IOException{
		Calendar calender = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");		
		
		TakesScreenshot ts = ((TakesScreenshot)driver);
		File image = ts.getScreenshotAs(OutputType.FILE);
		
		String actualImageName = "FailedScreenshot_"+formatter.format(calender.getTime())+".png";
		String path = System.getProperty("user.dir")+"\\FailedScreenShots\\"+actualImageName;
		File destFile = new File(path);
		//Final Copying the file to destination
		FileUtils.copyFile(image, destFile);
		return path;
	}

}
