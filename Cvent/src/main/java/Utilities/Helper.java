package Utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import Cvent.Cvent.Base;


public class Helper extends Base{

	public static boolean validateElementIsDisplayed(WebDriver driver, String xPath){

		WebDriverWait wait = new WebDriverWait(driver, 180);
		WebElement ele = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
		if(ele.isDisplayed()){
			return true;
		}
		else{
			return false;
		}		
	}

	//***********Select Dropdown**********/
	public static void SelectDrpDwn(WebElement DrpDwnElement, String value) {
		Select Sel = new Select(DrpDwnElement);	
		Sel.selectByValue(value);
	}
	public static void SelectDrpDwnText(WebElement DrpDwnElement, String text) {
		Select Sel = new Select(DrpDwnElement);	
		Sel.selectByVisibleText(text);
	}
	public static void SelectDrpDwnTextWithSpaceForFirstChild(WebElement DrpDwnElement, String text) {
		Select Sel = new Select(DrpDwnElement);	
		Sel.selectByVisibleText("   "+text);
	}
	public static void SelectDrpDwnTextWithSpaceForSecondChild(WebElement DrpDwnElement, String text) {
		Select Sel = new Select(DrpDwnElement);	
		Sel.selectByVisibleText("      "+text);
	}
	public static void SelectDrpBootStrap(WebElement ele){
		ele.click();
		
	}
	public static void SelectDrpDwnBootStrap(WebElement DrpDwnElement,WebElement TextContatingValue) {
		DrpDwnElement.click();
		Helper.ExplicitWait(TextContatingValue);
		TextContatingValue.click();
	}

	
	//********CheckBoxes*********

	public  static void Checkbox(WebElement e){

		if(e.isSelected()!= true){
			e.click();
		}
		else{			   
		}	   
	}

	//********Explicit Wait

	public static boolean ExplicitWait(WebElement ele){
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOf(ele));
		return true;
	}
	public static void ExplicitWaitVisibility(WebElement ele){
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.elementToBeClickable(ele));
	}



	//******Entering the content
	public static void ContentEntry(String DocumentName ) throws IOException{
		StringBuffer sb = new StringBuffer();
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"\\Data\\"+DocumentName);
		int data = fis.read();
		while(data!=-1){
			char c = ((char)data);
			data =fis.read();
			sb.append(c);	
			driver.switchTo().frame( driver.findElement(By.xpath("//iframe[@id = 'content_ifr']")));		
			WebElement Para =driver.findElement(By.xpath("//body[@id='tinymce']"));
			Para.click();
			Para.sendKeys(sb);
			ScreenShot();
			driver.switchTo().defaultContent();
		}
	}
	/*************Actions**********/

	public static void moveToElementAndClick(WebElement ele){
		Actions action=new Actions(driver);
		action.moveToElement(ele).click().build().perform();
	}

	public static void moveToElement(WebElement ele){
		Actions action=new Actions(driver);
		action.moveToElement(ele).build().perform();
	}

	/*************JAva Script Executor**********/
	public static void moveToElementJS(WebElement ele){
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].onmouseover()", ele); 
	}

	/**************Switch to New Window*****/
	public static void SwitchToWindow(int index) {		   
		Set<String> windowCount = driver.getWindowHandles();
		System.out.println("Number of open windows:-"+windowCount.size());
		LinkedList<String> windowsId = new LinkedList<String>(windowCount);

		if (index < 0 || index > windowsId.size()){
			throw new IllegalArgumentException("Invalid Index : " + index);
		}
		driver.switchTo().window(windowsId.get(index));			
	}
	/****************hIGHlIGHT eLEMENT*********/
	public static void highLightElement(WebElement element)
	{
		JavascriptExecutor js=(JavascriptExecutor)driver; 	    
		js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
		try 
		{
			Thread.sleep(1000);
		}
		catch (InterruptedException e) {	    
			System.out.println(e.getMessage());
		} 	    
		js.executeScript("arguments[0].setAttribute('style','border: solid 2px white');", element); 

	}

	/*********Highlight and Click*********/
	public static void highLightEleAndClick(WebElement element)
	{
		JavascriptExecutor js=(JavascriptExecutor)driver; 	    
		js.executeScript("arguments[0].setAttribute('style', 'background: turquoise; border: 2px solid red;');", element);
		try 
		{
			Thread.sleep(1000);
		}
		catch (InterruptedException e) {	    
			System.out.println(e.getMessage());
		} 	    
		js.executeScript("arguments[0].setAttribute('style','border: solid 2px white');", element); 
		element.click();
	}
	public static void highLightEleAndSendKeys(WebElement element, String text)
	{
		JavascriptExecutor js=(JavascriptExecutor)driver; 	    
		js.executeScript("arguments[0].setAttribute('style', 'background: turquoise; border: 2px solid red;');", element);
		try 
		{
			Thread.sleep(1000);
		}
		catch (InterruptedException e) {	    
			System.out.println(e.getMessage());
		} 	    
		js.executeScript("arguments[0].setAttribute('style','border: solid 2px white');", element); 
		element.sendKeys(text);
	}

	
}
