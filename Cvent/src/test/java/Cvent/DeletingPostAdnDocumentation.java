package Cvent;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Cvent.Cvent.AdminLoginPage;
import Cvent.Cvent.Base;
import Cvent.Cvent.DeletePosts;

public class DeletingPostAdnDocumentation extends Base {
	
	 WebDriver driver ;
	 
	 AdminLoginPage adminLoginPage = null;
		DeletePosts deletePosts = null;
		
		  @BeforeMethod
		  public void beforeMethod() {
			  init();
			  driver = Base.getBrowser(CONFIG.getProperty("browser"));
			  driver.get(CONFIG.getProperty("url"));	  
		  }
		  
		  @Test(priority =3, description="Delete all the Posts and Documentation/Documentation Categories created")
		  public void TestDeleteAllThePostsCreated() throws IOException  {
			  
			  adminLoginPage = PageFactory.initElements(driver, AdminLoginPage.class);
			  Assert.assertTrue(adminLoginPage.LoginMethod(CONFIG.getProperty("editor"), CONFIG.getProperty("pwd")));  	  
			  deletePosts =PageFactory.initElements(driver, DeletePosts.class);
			  Assert.assertTrue(deletePosts.PostDeleteMtd());	
		  }
		  
		  
		  @AfterMethod
		  public void afterMethod() {
		  driver.quit();
		  }

}
