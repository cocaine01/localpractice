package Cvent;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;

import Cvent.Cvent.AdminLoginPage;
import Cvent.Cvent.Base;
import Cvent.Cvent.LandingPage;
import Cvent.Cvent.PostCreation;

public class NewTest extends Base {
	
	WebDriver driver;
	
	AdminLoginPage AP = null;
	LandingPage LP = null;
	PostCreation PC = null;

  @BeforeMethod
  public void test00() {
	  init();
	  driver = Base.getBrowser(CONFIG.getProperty("browser"));
	  driver.get(CONFIG.getProperty("url"));
	  
  }
  
  @Test(priority=1 ,description = "Login to application")
  public void test01() throws IOException {
//	   AP = new AdminLoginPage(driver);
//	   LP =AP.LoginMethod(CONFIG.getProperty("u_name"), CONFIG.getProperty("pwd"));
//	   LP.CreateUserMtd();	
	 
	  AdminLoginPage adminLoginPageObj = PageFactory.initElements(driver, AdminLoginPage.class);
	  //Assert.assertTrue(adminLoginPageObj.LoginMethod(u_name, pwd));
	  Assert.assertTrue(adminLoginPageObj.LoginMethod(CONFIG.getProperty("u_name"), CONFIG.getProperty("pwd")));
//	  PostCreation PostCreationPageObj = PageFactory.initElements(driver, PostCreation.class);
//	  Assert.assertTrue(PostCreationPageObj.PostCreationMtd());
  }
  
//  @Test(description = "Create post", dependsOnMethods = {"test01"})
//  public void test02(){
//	  PostCreation PostCreationPageObj = PageFactory.initElements(driver, PostCreation.class);
//	  Assert.assertTrue(PostCreationPageObj.PostCreationMtd());
//	  
//  }
  
  @Test(priority=2 ,description= "Post Creation with Tech Writer Login")
  public void test02() throws IOException{
	  AdminLoginPage adminLoginPageObj = PageFactory.initElements(driver, AdminLoginPage.class);
	  Assert.assertTrue(adminLoginPageObj.LoginMethod(CONFIG.getProperty("tech_writer"), CONFIG.getProperty("pwd")));
  }
  
  @AfterMethod
  public void test99(){
	  driver.quit();
  }
  
  
  
  
  

}
