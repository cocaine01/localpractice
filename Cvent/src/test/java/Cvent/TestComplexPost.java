package Cvent;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Cvent.Cvent.AdminLoginPage;
import Cvent.Cvent.Base;
import Cvent.Cvent.ComplexDocCreation;

public class TestComplexPost extends Base {
	
	 WebDriver driver ;
	 
	 AdminLoginPage adminLoginPage = null;
	 ComplexDocCreation complexDocCreation = null;
	 
	 @Test(priority =1, description="Complex Doc Creation with Tech Writer Login")
	  public void Test01PostCreationWithTechWriter() throws IOException, InterruptedException  {
		  
		  adminLoginPage = PageFactory.initElements(driver, AdminLoginPage.class);
		  Assert.assertTrue(adminLoginPage.LoginMethod(CONFIG.getProperty("tech_writer"), CONFIG.getProperty("pwd")));	  
		  complexDocCreation= PageFactory.initElements(driver, ComplexDocCreation.class);
		  Assert.assertTrue(complexDocCreation.ComplexDocumentCreationMtd());	  
		  
	  }
	 
	 
	 @BeforeMethod
	  public void beforeMethod() {
		  init();
		  driver = Base.getBrowser(CONFIG.getProperty("browser"));
		  driver.get(CONFIG.getProperty("url"));	  
	  }
	  
	 @AfterMethod
	  public void afterMethod() {
      driver.quit();
	  }
	 
}