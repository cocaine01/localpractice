package Cvent;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

import Cvent.Cvent.AdminLoginPage;
import Cvent.Cvent.Base;
import Cvent.Cvent.BasicPostCreation;
import Cvent.Cvent.ContentOwnerCapabilities;

public class TestContentOwnerCapabilities extends Base {
	
	 WebDriver driver ;	 
	 AdminLoginPage adminLoginPage = null;
	 BasicPostCreation basicPostCreation = null;
	 ContentOwnerCapabilities ContentOwnerCapabilities = null;
	 
	 @BeforeMethod
	  public void beforeMethod() {
		  init();
		  driver = Base.getBrowser(CONFIG.getProperty("browser"));
		  driver.get(CONFIG.getProperty("url"));
		  //System.out.println("on navigation" + driver.getWindowHandle());
		  
	  }
	 
  @Test
  public void TestContentOwnerCapabilitiesTest() throws IOException {
	  adminLoginPage = PageFactory.initElements(driver, AdminLoginPage.class);
	  Assert.assertTrue(adminLoginPage.LoginMethod(CONFIG.getProperty("content_owner"), CONFIG.getProperty("pwd")));
	  
	  basicPostCreation= PageFactory.initElements(driver, BasicPostCreation.class);
	  Assert.assertTrue(basicPostCreation.PostCreationMtd(CONFIG.getProperty("heading_content_owner")));
	  
	  adminLoginPage = PageFactory.initElements(driver, AdminLoginPage.class);
	  Assert.assertTrue(adminLoginPage.LoginMethod(CONFIG.getProperty("content_owner"), CONFIG.getProperty("pwd")));
	  
	  ContentOwnerCapabilities = PageFactory.initElements(driver, ContentOwnerCapabilities.class);
	  Assert.assertTrue(ContentOwnerCapabilities.ContentOwnerCapabilitiesMtd(CONFIG.getProperty("heading_content_owner")));
  }


  @AfterMethod
  public void afterMethod() {
  driver.quit();
  }

}
