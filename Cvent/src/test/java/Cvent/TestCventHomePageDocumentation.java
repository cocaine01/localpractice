package Cvent;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import Cvent.Cvent.Base;
import Cvent.Cvent.BreakingDocumentation;



public class TestCventHomePageDocumentation extends Base {
	
	 WebDriver driver;
	 
	 BreakingDocumentation breakingDocumentation = null;
	 
	
	 @BeforeMethod
	  public void beforeMethod() {
		  init();
		  driver = Base.getBrowser(CONFIG.getProperty("browser"));
		  driver.get(CONFIG.getProperty("ProdHomeUrl"));	  
	  }
	 
	 @org.testng.annotations.Test
	 public void Test() throws InterruptedException{
		 BreakingDocumentation breakingDocumentation =PageFactory.initElements(driver, BreakingDocumentation.class);
		 Assert.assertTrue(breakingDocumentation.BrkMethod());
		 
	 }
	  
	 @AfterMethod
	  public void afterMethod() {
	  driver.quit();
	  }
	 
	
	

}
