package Cvent;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

import Cvent.Cvent.AdminLoginPage;
import Cvent.Cvent.Base;
import Cvent.Cvent.DocumentCreationHierarchy;
import Cvent.Cvent.DocumentationCategoryCreation;

public class TestDocumentHierarchy extends Base{
	
	
	 WebDriver driver ;	 
	 AdminLoginPage adminLoginPage = null;
	 DocumentationCategoryCreation DocumentationCategoryCreation =null ;
	 DocumentCreationHierarchy DocumentCreationHierarchy = null;
	 
	 
  @Test
  public void DocumentCreation() throws IOException, InterruptedException {
	  
	  adminLoginPage = PageFactory.initElements(driver, AdminLoginPage.class);
	  Assert.assertTrue(adminLoginPage.LoginMethod(CONFIG.getProperty("admin"), CONFIG.getProperty("pwd")));
	  
//	  DocumentationCategoryCreation = PageFactory.initElements(driver, DocumentationCategoryCreation.class);
//	  Assert.assertTrue(DocumentationCategoryCreation.DocumentCategoryCreation());
	  
	  DocumentCreationHierarchy = PageFactory.initElements(driver,DocumentCreationHierarchy.class);
	  Assert.assertTrue(DocumentCreationHierarchy.DocumentCategoryCreationHierarchy());
	  
	   }
  @BeforeMethod
 
	  public void beforeMethod() {
		  init();
		  driver = Base.getBrowser(CONFIG.getProperty("browser"));
		  driver.get(CONFIG.getProperty("url"));
		  
	  }
  

  @AfterMethod
  public void afterClass() {
	  //driver.quit();
  }

}
