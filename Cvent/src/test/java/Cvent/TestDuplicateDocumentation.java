package Cvent;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Cvent.Cvent.AdminLoginPage;
import Cvent.Cvent.Base;
import Cvent.Cvent.BasicPostCreation;
import Cvent.Cvent.DuplicateDocumentation;

public class TestDuplicateDocumentation extends Base {
	 WebDriver driver ;	 
	 AdminLoginPage adminLoginPage = null;
	 BasicPostCreation basicPostCreation = null;
	 DuplicateDocumentation duplicateDocumentation =null;
	 
	 
	 
	 @Test
	 public void DuplicatePostMtdTest() throws IOException, InterruptedException{
		  adminLoginPage = PageFactory.initElements(driver, AdminLoginPage.class);
		  Assert.assertTrue(adminLoginPage.LoginMethod(CONFIG.getProperty("editor"), CONFIG.getProperty("pwd")));
		  duplicateDocumentation = PageFactory.initElements(driver, DuplicateDocumentation.class);
		  Assert.assertTrue(duplicateDocumentation.TestDuplicatePostMtd());
	 }
	 
	 @BeforeMethod
	  public void beforeMethod() {
		  init();
		  driver = Base.getBrowser(CONFIG.getProperty("browser"));
		  driver.get(CONFIG.getProperty("url"));
		  
	  }
	 
	  @AfterMethod
	  public void afterMethod() {
	  driver.quit();
	  }
}
