package Cvent;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

import Cvent.Cvent.AdminLoginPage;
import Cvent.Cvent.Base;
import Cvent.Cvent.BasicPostCreation;
import Cvent.Cvent.EditorCapabilities;

public class TestEditorCapabilities extends Base {
	
	 WebDriver driver ;	 
	 AdminLoginPage adminLoginPage = null;
	 BasicPostCreation basicPostCreation = null;
	 EditorCapabilities EditorCapabilities = null;
	 
	 @BeforeMethod
	  public void beforeMethod() {
		  init();
		  driver = Base.getBrowser(CONFIG.getProperty("browser"));
		  driver.get(CONFIG.getProperty("url"));
		  //System.out.println("on navigation" + driver.getWindowHandle());
		  
	  }
	 
  @Test
  public void TestEditorCapabilitiesTest() throws IOException {
	  adminLoginPage = PageFactory.initElements(driver, AdminLoginPage.class);
	  Assert.assertTrue(adminLoginPage.LoginMethod(CONFIG.getProperty("editor"), CONFIG.getProperty("pwd")));
	  basicPostCreation= PageFactory.initElements(driver, BasicPostCreation.class);
	  Assert.assertTrue(basicPostCreation.PostCreationMtd(CONFIG.getProperty("heading_editor")));
	  adminLoginPage = PageFactory.initElements(driver, AdminLoginPage.class);
	  Assert.assertTrue(adminLoginPage.LoginMethod(CONFIG.getProperty("editor"), CONFIG.getProperty("pwd")));
	  EditorCapabilities = PageFactory.initElements(driver, EditorCapabilities.class);
	  Assert.assertTrue(EditorCapabilities.EditorCapabilitiesMtd(CONFIG.getProperty("heading_editor")));
  }


  @AfterMethod
  public void afterMethod() {
  driver.quit();
  }

}
