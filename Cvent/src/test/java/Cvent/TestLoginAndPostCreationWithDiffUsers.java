package Cvent;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

import Cvent.Cvent.AdminLoginPage;
import Cvent.Cvent.Base;
import Cvent.Cvent.BasicPostCreation;
import Cvent.Cvent.DeletePosts;
import Utilities.Helper;

public class TestLoginAndPostCreationWithDiffUsers extends Base{
	
 WebDriver driver ;
 
    AdminLoginPage adminLoginPage = null;
	BasicPostCreation basicPostCreation = null;
	DeletePosts deletePosts = null;
 
	//CONFIG.getProperty("")
 
  @BeforeMethod
  public void beforeMethod() {
	  init();
	  driver = Base.getBrowser(CONFIG.getProperty("browser"));
	  driver.get(CONFIG.getProperty("url"));	  
  }
  
  @Test(priority =1, description="Post Creation with Tech Writer Login")
  public void Test01PostCreationWithTechWriter() throws IOException  {
	  
	  adminLoginPage = PageFactory.initElements(driver, AdminLoginPage.class);
	  Assert.assertTrue(adminLoginPage.LoginMethod(CONFIG.getProperty("tech_writer"), CONFIG.getProperty("pwd")));	  
	  basicPostCreation= PageFactory.initElements(driver, BasicPostCreation.class);
	  Assert.assertTrue(basicPostCreation.PostCreationMtd(CONFIG.getProperty("heading_tech_writer")));	  
	  
  }
  
  @Test(priority =2, description="Post Creation with ContentOwner Login")
  public void Test02PostCreationWithContentOwner() throws IOException  {
	  adminLoginPage = PageFactory.initElements(driver, AdminLoginPage.class);
	  Assert.assertTrue(adminLoginPage.LoginMethod(CONFIG.getProperty("content_owner"), CONFIG.getProperty("pwd")));
	  basicPostCreation= PageFactory.initElements(driver, BasicPostCreation.class);
	  Assert.assertTrue(basicPostCreation.PostCreationMtd(CONFIG.getProperty("heading_content_owner")));	  
	  
  }
  
  @Test(priority =3, description="Post Creation with Editor Login")
  public void Test03PostCreationWithEditor() throws IOException  {
	  adminLoginPage = PageFactory.initElements(driver, AdminLoginPage.class);
	  Assert.assertTrue(adminLoginPage.LoginMethod(CONFIG.getProperty("editor"), CONFIG.getProperty("pwd")));
	  basicPostCreation= PageFactory.initElements(driver, BasicPostCreation.class);
	  Assert.assertTrue(basicPostCreation.PostCreationMtd(CONFIG.getProperty("heading_editor")));	  
	  
  }
  
  
  @AfterMethod
  public void afterMethod() {
  driver.quit();
  }

}
