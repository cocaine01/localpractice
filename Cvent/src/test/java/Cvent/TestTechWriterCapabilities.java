package Cvent;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;

import Cvent.Cvent.AdminLoginPage;
import Cvent.Cvent.Base;
import Cvent.Cvent.BasicPostCreation;
import Cvent.Cvent.TechWriterCapabilities;

public class TestTechWriterCapabilities extends Base {
	
	 WebDriver driver ;	 
	 AdminLoginPage adminLoginPage = null;
	 BasicPostCreation basicPostCreation = null;
	 TechWriterCapabilities TechWriterCapabilities = null;
	 
	 @BeforeMethod
	  public void beforeMethod() {
		  init();
		  driver = Base.getBrowser(CONFIG.getProperty("browser"));
		  driver.get(CONFIG.getProperty("url"));
		  //System.out.println("on navigation" + driver.getWindowHandle());
		  
	  }
	 
  @Test
  public void TestTechWriterCapabilitiesTest() throws IOException {
	  adminLoginPage = PageFactory.initElements(driver, AdminLoginPage.class);
	  Assert.assertTrue(adminLoginPage.LoginMethod(CONFIG.getProperty("tech_writer"), CONFIG.getProperty("pwd")));
	  basicPostCreation= PageFactory.initElements(driver, BasicPostCreation.class);
	  Assert.assertTrue(basicPostCreation.PostCreationMtd(CONFIG.getProperty("heading_tech_writer")));
	  adminLoginPage = PageFactory.initElements(driver, AdminLoginPage.class);
	  Assert.assertTrue(adminLoginPage.LoginMethod(CONFIG.getProperty("tech_writer"), CONFIG.getProperty("pwd")));
	  TechWriterCapabilities = PageFactory.initElements(driver, TechWriterCapabilities.class);
	  Assert.assertTrue(TechWriterCapabilities.TechWriterCapabilitiesMtd(CONFIG.getProperty("heading_tech_writer")));
  }


  @AfterMethod
  public void afterMethod() {
  driver.quit();
  }

}
