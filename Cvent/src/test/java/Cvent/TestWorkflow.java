package Cvent;

import java.io.IOException;

import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Cvent.Cvent.AdminLoginPage;
import Cvent.Cvent.Base;
import Cvent.Cvent.BasicPostCreation;

public class TestWorkflow extends Base {
	

	@BeforeMethod
	public void setUp(){
		  init();
		  driver = Base.getBrowser(CONFIG.getProperty("browser"));
		  driver.get(CONFIG.getProperty("url"));
	}
	
  @Test
  public void Test01Workflow() throws IOException {
	  AdminLoginPage adminLoginPageObj = PageFactory.initElements(driver, AdminLoginPage.class);
	  Assert.assertTrue(adminLoginPageObj.LoginMethod(CONFIG.getProperty("u_name"), CONFIG.getProperty("pwd")));
	  BasicPostCreation BasicPostCreation =PageFactory.initElements(driver, BasicPostCreation.class);
	  //Assert.assertTrue(BasicPostCreation.PostCreationMtd());
  }
  
  
  @AfterMethod
  public void closeBrowser(){
  driver.quit();
  }
}
